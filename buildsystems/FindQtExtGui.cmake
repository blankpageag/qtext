find_package(Qt4)
find_package(QtExt)

find_path(QtExtGui_INCLUDE_DIR MediaPlayer ${QtExt_INCLUDE_DIR} ${CMAKE_INSTALL_PREFIX}/include/qtext ${CMAKE_INSTALL_PREFIX}/include /usr/include/qtext /usr/local/include/qtext ${QT_INCLUDE_DIR}/qtext /usr/include /usr/local/include ${QT_INCLUDE_DIR})

if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
	set(QTEXTGUI_LIB_NAME "QtExtGui-d")
else()
	set(QTEXTGUI_LIB_NAME "QtExtGui")
endif()

find_library(QtExtGui_LIBRARY NAMES ${QTEXTGUI_LIB_NAME} PATHS ${CMAKE_INSTALL_PREFIX}/lib64 ${CMAKE_INSTALL_PREFIX}/lib ${CMAKE_INSTALL_PREFIX}/Library/Frameworks)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(${QTEXTGUI_LIB_NAME} DEFAULT_MSG QtExtGui_LIBRARY QtExtGui_INCLUDE_DIR)

if(QtExtGui_FOUND)
	set(QtExtGui_LIBRARIES ${QtExtGui_LIBRARY})
endif()

mark_as_advanced(QtExtGui_LIBRARY QtExtGui_INCLUDE_DIR)
