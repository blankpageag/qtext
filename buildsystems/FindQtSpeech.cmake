find_package(Qt4)
find_package(QtExt)

find_path(QtSpeech_INCLUDE_DIR Speech ${QtExt_INCLUDE_DIR} ${CMAKE_INSTALL_PREFIX}/include/qtext ${CMAKE_INSTALL_PREFIX}/include /usr/include/qtext /usr/local/include/qtext ${QT_INCLUDE_DIR}/qtext /usr/include /usr/local/include ${QT_INCLUDE_DIR})
find_library(QtSpeech_LIBRARY NAMES QtSpeech PATHS ${CMAKE_INSTALL_PREFIX}/lib64 ${CMAKE_INSTALL_PREFIX}/lib ${CMAKE_INSTALL_PREFIX}/Library/Frameworks)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(QtSpeech DEFAULT_MSG QtSpeech_LIBRARY QtSpeech_INCLUDE_DIR)

if(QtSpeech_FOUND)
	set(QtSpeech_LIBRARIES ${QtSpeech_LIBRARY})
endif()

mark_as_advanced(QtSpeech_LIBRARY QtSpeech_INCLUDE_DIR)
