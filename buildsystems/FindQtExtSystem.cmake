find_package(Qt4)
find_package(QtExt)

find_path(QtExtSystem_INCLUDE_DIR NetworkInterface ${QtExt_INCLUDE_DIR} ${CMAKE_INSTALL_PREFIX}/include/qtext ${CMAKE_INSTALL_PREFIX}/include /usr/include/qtext /usr/local/include/qtext ${QT_INCLUDE_DIR}/qtext /usr/include /usr/local/include ${QT_INCLUDE_DIR})

if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
	set(QTEXT_SYSTEM_LIB_NAME "QtExtSystem-d")
else()
	set(QTEXT_SYSTEM_LIB_NAME "QtExtSystem")
endif()

find_library(QtExtSystem_LIBRARY NAMES ${QTEXT_SYSTEM_LIB_NAME} PATHS ${CMAKE_INSTALL_PREFIX}/lib64 ${CMAKE_INSTALL_PREFIX}/lib ${CMAKE_INSTALL_PREFIX}/Library/Frameworks)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(QtExtSystem DEFAULT_MSG QtExtSystem_LIBRARY QtExtSystem_INCLUDE_DIR)

if(QtExtSystem_FOUND)
	set(QtExtSystem_LIBRARIES ${QtExtSystem_LIBRARY})
endif()

mark_as_advanced(QtExtSystem_LIBRARY QtExtSystem_INCLUDE_DIR)
