find_package(Qt4)

find_path(QtExt_INCLUDE_DIR QIOStream ${CMAKE_INSTALL_PREFIX}/include/qtext ${CMAKE_INSTALL_PREFIX}/include /usr/include/qtext /usr/local/include/qtext ${QT_INCLUDE_DIR}/qtext /usr/include /usr/local/include ${QT_INCLUDE_DIR})

if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
	set(QTEXT_LIB_NAME "QtExt-d")
else()
	set(QTEXT_LIB_NAME "QtExt")
endif()

find_library(QtExt_LIBRARY NAMES ${QTEXT_LIB_NAME} PATHS ${CMAKE_INSTALL_PREFIX}/lib64 ${CMAKE_INSTALL_PREFIX}/lib ${CMAKE_INSTALL_PREFIX}/Library/Frameworks)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(${QTEXT_LIB_NAME} DEFAULT_MSG QtExt_LIBRARY QtExt_INCLUDE_DIR)

if(QtExt_FOUND)
	set(QtExt_LIBRARIES ${QtExt_LIBRARY})
endif()

mark_as_advanced(QtExt_LIBRARY QtExt_INCLUDE_DIR)
