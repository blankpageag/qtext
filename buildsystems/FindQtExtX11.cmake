find_package(Qt4)
find_package(QtExt)

find_path(QtExtX11_INCLUDE_DIR QX11Tools ${QTEXT_INCLUDE_DIR} ${CMAKE_INSTALL_PREFIX}/include/qtext ${CMAKE_INSTALL_PREFIX}/include /usr/include/qtext /usr/local/include/qtext ${QT_INCLUDE_DIR}/qtext /usr/include /usr/local/include ${QT_INCLUDE_DIR})
find_library(QtExtX11_LIBRARY NAMES QtExtX11 PATHS ${CMAKE_INSTALL_PREFIX}/lib64 ${CMAKE_INSTALL_PREFIX}/lib ${CMAKE_INSTALL_PREFIX}/Library/Frameworks)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(QtExtX11 DEFAULT_MSG QtExtX11_LIBRARY QtExtX11_INCLUDE_DIR)

if(QtExtX11_FOUND)
	set(QtExtX11_LIBRARIES ${QtExtX11_LIBRARY})
endif()

mark_as_advanced(QtExtX11_LIBRARY QtExtX11_INCLUDE_DIR)
