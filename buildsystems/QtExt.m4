AC_DEFUN([ARK_PATH_QT4], [
	AC_ARG_WITH(qt, AS_HELP_STRING([--with-qt=DIR], [Location of Qt 4.x]))
	if test "$with_qt" = "yes" -o "$with_qt" = "no"; then
		ark_cv_lib_qt_dir="/this/does/not/exist"
	else
		ark_cv_lib_qt_dir="$with_qt"
	fi
	AC_ARG_ENABLE(qt_debug, AS_HELP_STRING([--enable-qt-debug], [Build with debugging versions of Qt libs]))
	AC_MSG_CHECKING([for Qt 4.x])
	LibDir="lib"
	if uname -m |grep -q 64 && test -d /usr/lib64; then
		LibDir="lib64"
	fi
	if ! test -d "$ark_cv_lib_qt_dir"; then
		ark_cv_lib_qt_dir=no
		for dir in $with_qt $QTDIR /usr/$LibDir/qt4 /opt/qt4 /usr/$LibDir/qt /opt/qt /usr/local/Trolltech/Qt-4.* /Library/Frameworks /c/qt; do
			if test -e $dir/lib/libQtCore.so -o -e $dir/lib/libQtCore.a -o -d $dir/lib/QtCore.framework -o -e $dir/lib/libQtCore.dylib; then
				ark_cv_lib_qt_dir="$dir"
				break
			elif test -e $dir/lib/QtCore4.dll; then
				# Yuck yuck yuck...
				ark_cv_qt_win32=yes
				ark_cv_lib_qt_dir="$dir"
				break
			fi
		done
		if test -d "$ark_cv_lib_qt_dir/lib/QtCore.framework"; then
			# Looks like OSX...
			ark_cv_qt_osx=yes
			CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include"
			LDFLAGS="$LDFLAGS -L$ark_cv_lib_qt_dir/lib -F$ark_cv_lib_qt_dir/lib"
		fi
	fi
	if test -x "$ark_cv_lib_qt_dir/bin/moc"; then
		MOC="$ark_cv_lib_qt_dir/bin/moc"
		UIC="$ark_cv_lib_qt_dir/bin/uic"
		RCC="$ark_cv_lib_qt_dir/bin/rcc"
	else
		MOC="moc"
		UIC="uic"
		RCC="rcc"
	fi
	if test "$ark_cv_lib_qt_dir" = "no"; then
		AC_MSG_RESULT([not found])
	else
		AC_MSG_RESULT([$ark_cv_lib_qt_dir])
		QT="$ark_cv_lib_qt_dir"
		AC_SUBST([QT])
		AC_SUBST([MOC])
		AC_SUBST([UIC])
		AC_SUBST([RCC])
	fi
])

AC_DEFUN([ARK_QT4_CORE], [
	AC_REQUIRE([ARK_PATH_QT4])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -D_REENTRANT -DQT_SHARED -I$ark_cv_lib_qt_dir/lib/QtCore.framework/Headers -I$ark_vc_lib_qt_dir/include/Qt"
		QTCORE="-framework QtCore"
	else
		CPPFLAGS="$CPPFLAGS -D_REENTRANT -DQT_SHARED -I$ark_cv_lib_qt_dir/include -I$ark_cv_lib_qt_dir/include/Qt -I$ark_cv_lib_qt_dir/include/QtCore"
		LDFLAGS="$LDFLAGS -L$ark_cv_lib_qt_dir/lib"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTCORE="-lQtCore4"
		else
			QTCORE="-lQtCore"
		fi
	fi
	if test "$enable_qt_debug" = "no"; then
		CPPFLAGS="$CPPFLAGS -DQT_NO_DEBUG"
	fi
	AC_SUBST(QTCORE)
])

AC_DEFUN([ARK_QT4_GUI], [
	AC_REQUIRE([ARK_QT4_CORE])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtGui.framework/Headers"
		QTGUI="-framework QtGui"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtGui"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTGUI="-lQtGui4"
		else
			QTGUI="-lQtGui"
		fi
	fi
	AC_SUBST(QTGUI)
])

AC_DEFUN([ARK_QT4_XML], [
	AC_REQUIRE([ARK_QT4_CORE])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtXml.framework/Headers"
		QTXML="-framework QtXml"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtXml"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTXML="-lQtXml4"
		else
			QTXML="-lQtXml"
		fi
	fi
	AC_SUBST(QTXML)
])

AC_DEFUN([ARK_QT4_DBUS], [
	AC_REQUIRE([ARK_QT4_XML])
	CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtDBus"
	if test "$ark_cv_qt_win32" = "yes"; then
		QTDBUS="-lQtDBus4"
	else
		QTDBUS="-lQtDBus"
	fi
	AC_SUBST(QTDBUS)
])

AC_DEFUN([ARK_QT4_NETWORK], [
	AC_REQUIRE([ARK_QT4_CORE])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtNetwork.framework/Headers"
		QTNETWORK="-framework QtNetwork"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtNetwork"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTNETWORK="-lQtNetwork4"
		else
			QTNETWORK="-lQtNetwork"
		fi
	fi
	AC_SUBST(QTNETWORK)
])

AC_DEFUN([ARK_QT4_OPENGL], [
	AC_REQUIRE([ARK_QT4_GUI])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtOpenGL.framework/Headers"
		QTOPENGL="-framework QtOpenGL"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtOpenGL"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTOPENGL="-lQtOpenGL4"
		else
			QTOPENGL="-lQtOpenGL"
		fi
	fi
	AC_SUBST(QTOPENGL)
])

AC_DEFUN([ARK_QT4_SCRIPT], [
	AC_REQUIRE([ARK_QT4_CORE])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtScript.framework/Headers"
		QTSCRIPT="-framework QtScript"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtScript"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTSCRIPT="-lQtScript4"
		else
			QTSCRIPT="-lQtScript"
		fi
	fi
	AC_SUBST(QTSCRIPT)
])

AC_DEFUN([ARK_QT4_SQL], [
	AC_REQUIRE([ARK_QT4_CORE])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtSql.framework/Headers"
		QTSQL="-framework QtSql"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtSql"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTSQL="-lQtSql4"
		else
			QTSQL="-lQtSql"
		fi
	fi
	AC_SUBST(QTSQL)
])

AC_DEFUN([ARK_QT4_SVG], [
	AC_REQUIRE([ARK_QT4_GUI])
	AC_REQUIRE([ARK_QT4_XML])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtSvg.framework/Headers"
		QTSVG="-framework QtSvg"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtSvg"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTSVG="-lQtSvg4"
		else
			QTSVG="-lQtSvg"
		fi
	fi
])

AC_DEFUN([ARK_QT4_TEST], [
	AC_REQUIRE([ARK_QT4_CORE])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtTest.framework/Headers"
		QTTEST="-framework QtTest"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtTest"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTTEST="-lQtTest4"
		else
			QTTEST="-lQtTest"
		fi
	fi
	AC_SUBST(QTTEST)
])

AC_DEFUN([ARK_QT4_COMPAT], [
	AC_REQUIRE([ARK_QT4_CORE])
	AC_REQUIRE([ARK_QT4_GUI])
	AC_REQUIRE([ARK_QT4_NETWORK])
	AC_REQUIRE([ARK_QT4_SQL])
	AC_REQUIRE([ARK_QT4_XML])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/Qt3Support.framework/Headers"
		QT3SUPPORT="-framework Qt3Support"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/Qt3Support"
		if test "$ark_cv_qt_win32" = "yes"; then
			QT3SUPPORT="-lQt3Support4"
		else
			QT3SUPPORT="-lQt3Support"
		fi
	fi
	AC_SUBST(QT3SUPPORT)
])

AC_DEFUN([ARK_QT4_PHONON], [
	AC_REQUIRE([ARK_QT4_GUI])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/phonon.framework/Headers"
		PHONON="-framework phonon"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/phonon -I$ark_cv_lib_qt_dir/include/Phonon -I$ark_cv_lib_qt_dir/include/phonon/Phonon"
		if test "$ark_cv_qt_win32" = "yes"; then
			PHONON="-lphonon4"
		else
			PHONON="-lphonon"
		fi
	fi
	AC_SUBST(PHONON)
])

AC_DEFUN([ARK_QT4_DESIGNER], [
	AC_REQUIRE([ARK_QT4_CORE])
	AC_REQUIRE([ARK_QT4_GUI])
	AC_REQUIRE([ARK_QT4_NETWORK])
	AC_REQUIRE([ARK_QT4_XML])
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtDesigner.framework/Headers"
		QTDESIGNER="-framework QtDesigner"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtDesigner"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTDESIGNER="-lQtDesigner4"
		else
			QTDESIGNER="-lQtDesigner"
		fi
	fi
	AC_SUBST(QTDESIGNER)
])

AC_DEFUN([ARK_QT_WEBKIT], [
	AC_REQUIRE([ARK_QT4_GUI])
	AC_MSG_CHECKING([for WebKit])
	SAVE_CPPFLAGS="$CPPFLAGS"
	SAVE_LDFLAGS="$LDFLAGS"
	if test "$ark_cv_qt_osx" = "yes"; then
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/lib/QtWebKit.framework/Headers"
		QTWEBKIT="-framework QtWebKit"
	else
		CPPFLAGS="$CPPFLAGS -I$ark_cv_lib_qt_dir/include/QtWebKit"
		if test "$ark_cv_qt_win32" = "yes"; then
			QTWEBKIT="-lQtWebKit4"
		else
			QTWEBKIT="-lQtWebKit"
		fi
	fi
	AC_LANG_PUSH([C++])
	AC_COMPILE_IFELSE([
		#include <qwebpage.h>
	], [found=yes], [found=no])
	AC_MSG_RESULT([$found])
	AC_LANG_POP
	if test "$found" = "yes"; then
		AC_DEFINE([HAVE_WEBKIT], [1], [Define if you have WebKit])
		AC_SUBST(QTWEBKIT)
	else
		CPPFLAGS="$SAVE_CPPFLAGS"
		LDFLAGS="$SAVE_LDFLAGS"
	fi
])

AC_DEFUN([ARK_QT4], [
	AC_REQUIRE([ARK_QT4_CORE])
	AC_REQUIRE([ARK_QT4_GUI])
	AC_REQUIRE([ARK_QT4_XML])
	AC_REQUIRE([ARK_QT4_NETWORK])
	AC_REQUIRE([ARK_QT4_OPENGL])
	AC_REQUIRE([ARK_QT4_DBUS])
	AC_REQUIRE([ARK_QT4_SCRIPT])
	AC_REQUIRE([ARK_QT4_SQL])
	AC_REQUIRE([ARK_QT4_SVG])
	AC_REQUIRE([ARK_QT4_TEST])
	AC_REQUIRE([ARK_QT4_PHONON])
])

AC_DEFUN([ARK_PATH_KDE4], [
	AC_ARG_WITH(kde, AS_HELP_STRING([--with-kde=DIR], [Location of KDE 4.x]))
	if test "$with_kde" = "yes"; then
		with_kde=""
	fi
	AC_MSG_CHECKING([for KDE 4.x])
	if test "$with_kde" = "no"; then
		AC_MSG_RESULT([disabled by user])
	else
		ark_cv_lib_kde_dir=no
		if uname -m |grep -q 64 && test -d /usr/lib64; then
			LibDir="lib64"
		fi
		for dir in $with_kde /opt/kde-4.0 /opt/kde-4.0.0 /opt/kde4 /usr; do
			if test -e $dir/$LibDir/libkhtml.so.5; then
				ark_cv_lib_kde_dir="$dir"
				break
			fi
		done
		if test "$ark_cv_lib_kde_dir" = "no"; then
			with_kde=no
			AC_MSG_RESULT([not found])
		else
			AC_DEFINE_UNQUOTED([WITH_KDE], [1], [Define to use KDE])
			AC_MSG_RESULT([$ark_cv_lib_kde_dir])
			KDE="$ark_cv_lib_kde_dir"
			if test "$KDE" = "/usr"; then
				KDE_INCLUDES="-I/usr/include/kde"
			else
				KDE_INCLUDES="-I$KDE/include"
			fi
			AC_SUBST([KDE])
			AC_SUBST([KDE_INCLUDES])
			KDECORE="-L$KDE/lib -Wl,--rpath -Wl,$KDE/lib -lkdecore"
			KDEUI="$KDECORE -lkdeui"
			KHTML="$KDECORE -lkhtml"
			AC_SUBST([KDECORE])
			AC_SUBST([KHTML])
			AC_SUBST([KDEUI])
		fi
	fi
	AM_CONDITIONAL(WITH_KDE, test "$with_kde" != "no")
])

AC_DEFUN([ARK_PATH_QTEXT], [
	AC_MSG_CHECKING([for Qt Extension library])
	AC_LANG_PUSH([C++])
	qtextdir=""
	for i in ${includedir} ${prefix}/include /usr/local/include /usr/include; do
		if test -d $i/qtext; then
			qtextdir="$i/qtext"
			break
		fi
	done
	if test -z "$qtextdir"; then
		AC_MSG_RESULT([not found])
	else
		AC_MSG_RESULT([headers in $qtextdir])
		CPPFLAGS="$CPPFLAGS -I$qtextdir"
	fi
	AC_LANG_POP
])

AC_DEFUN([ARK_QT_PLATFORM], [
	AC_REQUIRE([ARK_QT4_CORE])
	AC_MSG_CHECKING([Qt platform])
	platform=unknown
	AC_COMPILE_IFELSE([
		#include <qglobal.h>
		#ifndef Q_WS_X11
		#error Not X11
		#endif
	], [platform=X11], [])
	if test "$platform" = "unknown"; then
		AC_COMPILE_IFELSE([
			#include <qglobal.h>
			#ifndef Q_WS_QWS
			#error Not Qtopia
			#endif
		], [platform=Qtopia], [])
	fi
	if test "$platform" = "unknown"; then
		AC_COMPILE_IFELSE([
			#include <qglobal.h>
			#ifndef Q_WS_MACX
			#error Not OSX
			#endif
		], [platform=OSX], [])
	fi
	if test "$platform" = "unknown"; then
		AC_COMPILE_IFELSE([
			#include <qglobal.h>
			#ifndef Q_WS_MAC9
			#error Not OS9
			#endif
		], [platform=OS9], [])
	fi
	if test "$platform" = "unknown"; then
		AC_COMPILE_IFELSE([
			#include <qglobal.h>
			#ifndef Q_WS_WIN32
			#error No dirty Nazi junk
			#endif
		], [platform=Win32], [])
	fi
	AM_CONDITIONAL([QT_X11], [test "$platform" = "X11"])
	AM_CONDITIONAL([QTOPIA], [test "$platform" = "Qtopia"])
	AM_CONDITIONAL([QT_OSX], [test "$platform" = "OSX"])
	AM_CONDITIONAL([QT_OS9], [test "$platform" = "OS9"])
	AM_CONDITIONAL([QT_WIN32], [test "$platform" = "Win32"])
	AC_MSG_RESULT([$platform])
])
