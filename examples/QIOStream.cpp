// Example for QIOStream functions

#include <QIOStream>

using namespace std;

int main(int, char **) {
	QString s("Current");
	cerr << s << " date and time: " << QDateTime::currentDateTime() << endl;

	QByteArray a("Test");
	cerr << "Content of QByteArray: " << a << endl;
}
