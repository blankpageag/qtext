#include <QCoreApplication>
#include <Downloader>
#include <iostream>
using namespace std;

int main(int argc, char **argv) {
	QCoreApplication app(argc, argv);
	QByteArray b=Downloader::get(QUrl("http://arklinux.ch/"));
	cerr << b.data();
	b=Downloader::get(QUrl("http://rigi.blankpage.ch:1234/content"), 0, 0, 0, 48, 51);
	cerr << b.data() << endl; // should be "html"
	b=Downloader::get(QUrl("http://rigi.blankpage.ch:1234/content"), static_cast<QNetworkAccessManager*>(0), 0, 0, 48);
	cerr << b.data() << endl; // bytes 48 to end
}
