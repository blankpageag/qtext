#include <MD5>
#include <QIOStream>

using namespace std;

int main(int argc, char **argv) {
	QString hash=MD5::hash("GET:/dir/index.html");
	if(hash=="39aff3a2bab6126f332b942af96d3366")
		cout << "MD5 implementation matches example from Wikipedia Digest_access_authentication page" << endl;
	else
		cout << "MD5 implementation returns unexpected value: " << hash << endl;
}
