#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 2))
#include <SIMDVector>
#endif
#include <iostream>

using namespace std;

int main(int argc, char **argv) {
#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 2))
	SIMDVector<float,4> a, b, c;
	a[0] = 1.0; a[1] = 2.0; a[2] = 3.0 ; a[3] = 4.0;
	b[0] = 5.0; b[1] = 6.0; b[2] = 7.0 ; b[3] = 8.0;
	c = a + b;
	for(int i=0; i<4; i++) {
		cout << a[i] << " + " << b[i] << " = " << c[i] << endl;
	}
#else
#warning You need a newer gcc to use SIMDVector
#endif
}
