#include <QX11Tools>
#include <QApplication>
#include <QDesktopWidget>
#include <QRect>

#include <iostream>
using namespace std;

extern "C" {
#include <unistd.h>
}

int main(int argc, char **argv) {
	QApplication app(argc, argv);
	QRect r(app.desktop()->screenGeometry());
	cout << "Current resolution: " << r.width() << "x" << r.height() << endl;
	cout << "Switching to 640x480" << endl;
	QX11Tools::setResolution(640, 480);
	cout << "Waiting for 5 seconds" << endl;
	sleep(5);
	cout << "Restoring " << r.width() << "x" << r.height() << endl;
	QX11Tools::setResolution(r.size());
}
