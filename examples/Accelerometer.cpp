#include "AccelerometerApp"
#include <Accelerometer>
#include <QIOStream>

using namespace std;

AccelerometerApp::AccelerometerApp(Accelerometer *acc, int &argc, char **&argv):QCoreApplication(argc, argv) {
	connect(acc, SIGNAL(positionChanged(int,int,int)), this, SLOT(positionChanged(int,int,int)));
	connect(acc, SIGNAL(orientationChanged(Accelerometer::Orientation)), this, SLOT(orientationChanged(Accelerometer::Orientation)));
	acc->startTracking();
}

void AccelerometerApp::positionChanged(int x, int y, int z) {
	cout << "Position updated: (" << x << ", " << y << ", " << z << ")" << endl;
}

void AccelerometerApp::orientationChanged(Accelerometer::Orientation o) {
	cout << "Orientation changed!" << endl
		<< "\t" << ((o & Accelerometer::Portrait) ? "Portrait" : "Landscape") << ((o & Accelerometer::UpsideDown) ? ", upside down" : "") << endl;
}

int main(int argc, char **argv) {
	Accelerometer *a = Accelerometer::instance();
	if(!a) {
		cerr << "You don't have a supported accelerometer" << endl;
		return 1;
	}
	cout << "Current position: (" << a->x() << ", " << a->y() << ", " << a->z() << ")" << endl;

	AccelerometerApp app(a, argc, argv);
	app.exec();
}
