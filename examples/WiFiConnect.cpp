#include <WiFiInterface>
#include <QIOStream>
#include <QStringList>
#include <QProcess>

using namespace std;

int main(int argc, char **argv) {
	QString intf="wlan0";
	QString ssid=argv[1];
	if(argc>2) {
		intf=argv[1];
		ssid=argv[2];
	}
	NetworkInterface *i=NetworkInterface::get(intf);
	if(!i) {
		cerr << intf << " is not a network interface" << endl;
		return 1;
	}
	WiFiInterface *w=dynamic_cast<WiFiInterface*>(i);
	if(!w) {
		cerr << intf << " is not a wireless interface" << endl;
		return 2;
	}
	if(ssid.isEmpty()) {
		QList<WiFiNetwork*> nets=w->scan();
		WiFiNetwork *net=0;
		int strength = -1000;
		foreach(WiFiNetwork *n, nets) {
			if(!net || strength<n->signalStrength) {
				net=n;
				strength=n->signalStrength;
			}
		}
		if(!net) {
			cerr << "No networks found on " << intf << endl;
			return 3;
		}
		ssid=net->ssid;
	}
	w->up();
	w->connect(ssid);
	QProcess::execute("/bin/DHCP", QStringList() << intf);
	return 0;
}
