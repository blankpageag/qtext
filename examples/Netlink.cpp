#include <Netlink>
#include <QIOStream>

using namespace std;

int main(int argc, char **argv) {
	int acpiId=GeNetlink::id("acpi_event");
	if(acpiId<0) {
		cerr << "Your kernel is too old to send ACPI events over netlink." << endl;
		return 1;
	}
	cerr << "Your kernel sends ACPI events on channel " << acpiId << " using multicast group " << GeNetlink::mcastGroupID("acpi_event", "acpi_mc_group") << endl;
}
