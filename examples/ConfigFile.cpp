#include <ConfigFile>
#include <QIOStream>

using namespace std;

int main(int argc, char **argv)
{
	ConfigFile f("/usr/share/applications/kde/konsole.desktop");
	cout << "Konsole is a " << f["Desktop Entry"]["GenericName"] << endl;
}
