#include <System/NetworkInterface>
#include <System/WiFiInterface>
#include <QIOStream>

using namespace std;

int main(int argc, char **argv)
{
	QString interfaceName("eth0");
	if(argc>1)
		interfaceName=argv[1];
	NetworkInterface *interface=NetworkInterface::get(interfaceName);
	if(!interface || !interface->exists()) {
		cerr << "No such network interface" << endl;
		return 1;
	}
	cerr << interface->name() << " has MAC address " << interface->macAddress() << " and IP address " << interface->ip().toString() << " with netmask " << interface->netmask().toString() << " and broadcast address " << interface->broadcast().toString() << ". It is " << (interface->isUp() ? "up" : "down") << "." << endl;
	if(interface->isWireless()) {
		cerr << "It's wireless" << endl;
		WiFiInterface *w=static_cast<WiFiInterface*>(interface);
		QString nw=w->currentNetwork();
		if(!nw.isEmpty())
			cerr << "Currently connected to SSID " << w->currentNetwork() << endl;
		else
			cerr << "Currently not connected" << endl;
		cerr << "Available networks:" << endl;
		QList<WiFiNetwork*> networks=w->scan();
		foreach(WiFiNetwork* n, networks) {
			cerr << n->ssid << " signal strength: " << n->signalStrength << " rates: " << n->rates << endl;
		}
	}
	delete interface;

#if 0
	cerr << "Adding some weird route for testing..." << endl;
	if(!interface.addRoute(QHostAddress("192.168.99.0"), QHostAddress("255.255.255.0"), QHostAddress("212.147.80.110")))
		cerr << "Failed" << endl;
#endif
}
