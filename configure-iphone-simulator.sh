#! /bin/bash
unset CPATH
unset C_INCLUDE_PATH
unset CPLUS_INCLUDE_PATH
unset OBJC_INCLUDE_PATH
unset LIBS
unset DYLD_FALLBACK_LIBRARY_PATH
unset DYLD_FALLBACK_FRAMEWORK_PATH

if [ $# -gt 0 ]; then
	export SDKVER="$1"
else
	export SDKVER="5.1"
fi
export DEVROOT="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer"
export SDKROOT="$DEVROOT/SDKs/iPhoneSimulator$SDKVER.sdk"
export PKG_CONFIG_PATH="$SDKROOT/usr/lib/pkgconfig":"$SDKROOT/usr/share/pkgconfig"
export PKG_CONFIG_LIBDIR="$PKG_CONFIG_PATH"
export MAINFOLDER=`pwd`

mkdir build &>/dev/null
cd build
cmake .. $QTARGS -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS:STRING="-O2 -fweb -fomit-frame-pointer -frename-registers -fvisibility=hidden -fvisibility-inlines-hidden" -DCMAKE_CXX_FLAGS_RELEASE:STRING="-O2 -fweb -fomit-frame-pointer -frename-registers -fvisibility=hidden -fvisibility-inlines-hidden" -DCMAKE_TOOLCHAIN_FILE="$MAINFOLDER/buildsystems/iphone-simulator-$SDKVER.toolchain" -DCMAKE_SHARED_LINKER_FLAGS:STRING="-miphoneos-version-min=${SDKVER} --sysroot=${SDKROOT} -F${SDKROOT}/System/Library/Frameworks" -DCMAKE_MODULE_LINKER_FLAGS:STRING="-miphoneos-version-min=${SDKVER} --sysroot=${SDKROOT} -F${SDKROOT}/System/Library/Frameworks" -DCMAKE_EXE_LINKER_FLAGS:STRING="-miphoneos-version-min=${SDKVER} --sysroot=${SDKROOT} -F${SDKROOT}/System/Library/Frameworks" -DCMAKE_INSTALL_PREFIX="$SDKROOT/usr"
