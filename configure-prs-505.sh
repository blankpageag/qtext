#! /bin/bash
cd "`dirname $0`"

export TARGET=arm-9tdmi-linux-gnu
export PREFIX=/tmp/sd
export SDKROOT="/opt/crosstool/gcc-4.0.4-glibc-2.3.6/$TARGET"
export PKG_CONFIG_PATH="$SDKROOT/$TARGET/usr/lib/pkgconfig":"$SDKROOT/$TARGET/usr/share/pkgconfig":"$SDKROOT/$TARGET/lib/pkgconfig":"$SDKROOT"$PREFIX/lib/pkgconfig:"$SDKROOT"$PREFIX/share/pkgconfig
export PKG_CONFIG_LIBDIR="$PKG_CONFIG_PATH"
export MAINFOLDER=`pwd`
export QMAKESPEC=$SDKROOT/$TARGET/$PREFIX/mkspecs/qws/linux-arm-g++

rm -rf build
mkdir build
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE="$MAINFOLDER/buildsystems/$TARGET.toolchain" -DCMAKE_INSTALL_PREFIX="$PREFIX" -DWITH_SPEECH:BOOL=OFF -DSTATIC_EXAMPLES:BOOL=ON -DWITH_NETLINK:BOOL=OFF -DQT_RT_LIBRARY:FILEPATH=-lrt
make
make install DESTDIR="$SDKROOT/$TARGET"
