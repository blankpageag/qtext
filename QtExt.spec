%define beta %nil
%define date 48

Name: QtExt
Version: 0.0
%if "%beta" == ""
%if "%date" == ""
Release: 1ark
Source: %name-%version.tar.lz
%else
Release: 0.%date.1ark
Source: %name-%date.tar.lz
%endif
%else
%if "%date" == ""
Release: 0.%beta.1ark
Source: %name-%version%beta.tar.lz
%else
Release: 0.%beta.%date.1ark
Source: %name-%date.tar.lz
%endif
%endif
Summary: Library of Qt extensions
URL: http://www.arklinux.org/
License: GPL/Commercial licensing available for a fee
Group: System Environment/Libraries
BuildRoot: %_tmppath/%name-root

%description
Library of Qt extensions

%package devel
Summary: Development files for %name
Group: Development/Libraries
Requires: %name = %version-%release

%description devel
Development files (Headers etc.) for %name.

%package gui
Summary: GUI (Graphical User Interface) components of %name
Group: System/Libraries
Requires: %name = %version-%release

%package speech
Summary: Speech (TTS) related components of %name
Group: System/Libraries
Requires: %name = %version-%release

%description speech
Speech (TTS) related components of %name

%package x11
Summary: X11 related components of %name
Group: System/Libraries
Requires: %name = %version-%release

%description x11
X11 related components of %name

%package system
Summary: System related components of %name
Group: System/Libraries
Requires: %name = %version-%release

%description system
System related components of %name

%prep
%if "%date" == ""
%setup -q -n %name-%version%beta
%else
%setup -q -n %name
%endif
make -f Makefile.am
%configure --enable-speech

%build
make %?_smp_mflags

%install
rm -rf $RPM_BUILD_ROOT
make %?_smp_mflags install DESTDIR="$RPM_BUILD_ROOT"

%files
%defattr(-,root,root)
%_libdir/libQtExt.so*

%files devel
%defattr(-,root,root)
%_includedir/*
%_datadir/aclocal/*

%files speech
%defattr(-,root,root)
%_libdir/libQtSpeech.so*

%files gui
%defattr(-,root,root)
%_libdir/libQtExtGui.so*
%_datadir/qtext

%files x11
%defattr(-,root,root)
%_libdir/libQtExtX11.so*

%files system
%defattr(-,root,root)
%_libdir/libQtExtSystem.so*

%clean
rm -rf $RPM_BUILD_ROOT $RPM_BUILD_DIR/%name-%version

%changelog
* Thu Jul 17 2008 Bernhard Rosenkraenzer <bero@arklinux.org> 0.0-0.35.1ark
- Add new GUI components

* Wed Mar 12 2008 Bernhard Rosenkraenzer <bero@arklinux.org> 0.0-0.31.1ark
- Fix QtSpeech crash triggered by double linkage to festival static libs

* Tue Mar 11 2008 Bernhard Rosenkraenzer <bero@arklinux.org> 0.0-0.30.1ark
- Rebuild against Qt 4.4

* Thu Jan 17 2008 Bernhard Rosenkraenzer <bero@arklinux.org> 0.0-0.29.1ark
- Update, introduces Modules class and various bugfixes

* Tue Jul 10 2007 Bernhard Rosenkraenzer <bero@arklinux.org> 0.0-0.16.2ark
- Rebuild for gcc 4.2.1

* Sun May 20 2007 Bernhard Rosenkraenzer <bero@arklinux.org> 0.0-0.16.1ark
- Add XRandR support class
- Add platform detection code to autoconf scripts

* Mon Apr 23 2007 Bernhard Rosenkraenzer <bero@arklinux.org> 0.0-0.12.1ark
- initial RPM
