
@echo off

SET CMAKE_OUT_DIR=out
SET AKILI_DIR=c:/AkiliBooks
SET AKILI_BIN_DIR=%AKILI_DIR%/bin

SET BUILD_TYPE=Release

if "%1"=="debug" (
	SET BUILD_TYPE=Debug
)

ECHO "Build Type: %BUILD_TYPE%"

rmdir /S /Q %CMAKE_OUT_DIR%
mkdir %CMAKE_OUT_DIR%
cd  %CMAKE_OUT_DIR%
cmake -G "MinGW Makefiles" ..\ -DCMAKE_INSTALL_PREFIX=%AKILI_DIR% -DMPLAYER=%AKILI_BIN_DIR%/mplayer -DLIB_DESTINATION=%AKILI_BIN_DIR% -DCMAKE_BUILD_TYPE=%BUILD_TYPE%



cd ..
