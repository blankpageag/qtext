#include <QtTest/QtTest>
#include <ConfigFile>

class ConfigFileTest:public QObject {
	Q_OBJECT
private slots:
	void initTestCase();
	void cleanupTestCase();
	void testSimpleConfigFile();
	void testStructuredConfigFile();
	void testFallbackSystem();
};
