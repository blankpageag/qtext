#include "ConfigFileTest.h"

void ConfigFileTest::initTestCase() {
}

void ConfigFileTest::cleanupTestCase() {
}

void ConfigFileTest::testSimpleConfigFile() {
	ConfigFile f(TESTS "/simpletest.conf");
	QVERIFY(f.get("Linux") == "Good");
	QVERIFY(f.get("FreeBSD", QString::null, "Also good") == "Also good");
	QVERIFY(f.get("Windows") == "Bad");
	QVERIFY(f.get("xyz").isEmpty());
}

void ConfigFileTest::testStructuredConfigFile() {
	ConfigFile f(TESTS "/structuredtest.conf");
	QVERIFY(f["OS"]["Linux"] == "Good");
	QVERIFY(f.get("FreeBSD", "OS", "Also good") == "Also good");
	QVERIFY(f.get("Windows", "OS", "Awful") == "Bad");
	QVERIFY(f.get("xyz", "OS").isEmpty());
	QVERIFY(f["Numbers"].getInt("One") == 1);
	QVERIFY(f["Numbers"].getInt("Two") == 2);
}

void ConfigFileTest::testFallbackSystem() {
	ConfigFile fallback(TESTS "/fallbacktest.conf");
	ConfigFile f(TESTS "/structuredtest.conf", &fallback);
	QVERIFY(f["OS"]["Linux"] == "Good");
	QVERIFY(f.get("FreeBSD", "OS", "Also good") == "Also good");
	QVERIFY(f.get("Windows", "OS", "Awful") == "Bad");
	QVERIFY(f.get("xyz", "OS").isEmpty());
	QVERIFY(f["Numbers"].getInt("One") == 1);
	QVERIFY(f["Numbers"].getInt("Two") == 2);
	QVERIFY(f["Numbers"].getInt("Three") == 3);
}

QTEST_MAIN(ConfigFileTest)
