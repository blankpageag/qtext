#include <QX11Tools>
#include <QX11Info>

extern "C" {
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#include <X11/extensions/Xvlib.h>
}

bool QX11Tools::hasXvideo()
{
	unsigned int ver, rev, reqB, eventB, errorB;
	if(XvQueryExtension(QX11Info::display(), &ver, &rev, &reqB, &eventB, &errorB)) {
		unsigned int nadaptors;
		XvAdaptorInfo *ainfo;
		XvQueryAdaptors(QX11Info::display(), RootWindow(QX11Info::display(), 0), &nadaptors, &ainfo);
		return nadaptors>0;
	}
	return false;
}

bool QX11Tools::setResolution(int x, int y)
{
	int numSizes;
	Display *d=QX11Info::display();
	int const screen=QX11Info::appScreen();
	XRRScreenSize *sizes=XRRSizes(d, screen, &numSizes);
	for(int i=0; i<numSizes; i++) {
		if(sizes[i].width == x && sizes[i].height == y) {
			Rotation rot;
			XRRScreenConfiguration *cf=XRRGetScreenInfo(d, QX11Info::appRootWindow());
			SizeID size=XRRConfigCurrentConfiguration(cf, &rot);
			Status s=XRRSetScreenConfig(d, cf, DefaultRootWindow(d), (SizeID)i, rot, CurrentTime);
			XRRFreeScreenConfigInfo(cf);
			return s==RRSetConfigSuccess;
		}
	}
	return false;
};
