#include "ConfigFile"
#include <QFile>
#include <QTextStream>
#include <cstdlib>

static QString unquote(QString const &s) {
	if((s.startsWith('"') && s.endsWith('"')) || (s.startsWith('\'') && s.endsWith('\'')))
		return s.mid(1, s.length()-2);
	return s;
}

QString ConfigSection::get(QString key, QString const &dflt, bool localized) const
{
	while(localized) {
		QString locale=getenv("LANG");
		if(!locale.isEmpty()) {
			QString s;
			#define CHECKLOCALE \
				s=key + "[" + locale + "]"; \
				if(contains(s)) { \
					key=s; \
					break; \
				}

			CHECKLOCALE
			if(locale.contains('@')) {
				locale=locale.section('@', 0, 0);
				CHECKLOCALE
			}
			if(locale.contains('.')) {
				locale=locale.section('.', 0, 0);
				CHECKLOCALE
			}
			if(locale.contains('_')) {
				locale=locale.section('_', 0, 0);
				CHECKLOCALE
			}
			#undef CHECKLOCALE
		}
		break;
	}
	if(_fallback && !contains(key))
		return _fallback->get(key, _fallbackSection, dflt, localized);
	return value(key, dflt);
}

int ConfigSection::getInt(QString const &key, int const &dflt, bool localized) const
{
	return get(key, QString::number(dflt), localized).toInt();
}

double ConfigSection::getDouble(QString const &key, double const &dflt, bool localized) const
{
	return get(key, QString::number(dflt), localized).toDouble();
}

QStringList ConfigSection::getStringList(QString const &key, QString const &sep, QStringList const &dflt, bool localized) const
{
	return get(key, dflt.join(sep), localized).split(sep);
}

bool ConfigSection::getBool(QString const &key, bool const &dflt, bool localized) const
{
	QString v=get(key, QString::null, localized).toLower();
	if(v.isEmpty())
		return dflt;
	return v=="yes" || v=="on" || v=="true" || v=="1";
}

ConfigFile::ConfigFile(QString const &filename, ConfigFile const * const fallback):_fallback(fallback)
{
	QString section=QString::null;
	QFile f(filename);
	if(!f.open(QFile::ReadOnly))
		return;

	QTextStream t(&f);
	while(!t.atEnd()) {
		QString s=t.readLine();
		if(s.startsWith("#"))
			continue;
		else if(s.startsWith("[") && s.endsWith("]")) {
			section=s.mid(1).left(s.length()-2);
			if(!contains(section) && fallback && fallback->contains(section))
				insert(section, ConfigSection(fallback, section));
		} else if(s.contains("="))
			(*this)[section].insert(s.section('=', 0, 0), unquote(s.section('=', 1, -1)));
	}
	f.close();
}

QString ConfigFile::get(QString key, QString const &section, QString const &dflt, bool localized) const
{
	if(!contains(section)) {
		if(_fallback && _fallback->contains(section))
			return _fallback->get(key, section, dflt, localized);
		return dflt;
	}
	return value(section).get(key, dflt, localized);
}

int ConfigFile::getInt(QString const &key, QString const &section, int const &dflt, bool localized) const
{
	return get(key, section, QString::number(dflt), localized).toInt();
}

double ConfigFile::getDouble(QString const &key, QString const &section, double const &dflt, bool localized) const
{
	return get(key, section, QString::number(dflt), localized).toDouble();
}

bool ConfigFile::getBool(QString const &key, QString const &section, bool const &dflt, bool localized) const
{
	if(!contains(section)) {
		if(_fallback && _fallback->contains(section))
			return _fallback->getBool(key, section, dflt, localized);
		return dflt;
	}
	return value(section).getBool(key, dflt, localized);
}

QStringList ConfigFile::getStringList(QString const &key, QString const &section, QString const &sep, QStringList const &dflt, bool localized) const
{
	return get(key, section, dflt.join(sep), localized).split(sep);
}
