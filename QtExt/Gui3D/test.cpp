#include "PageFlipView"
#include <QIOStream>

using namespace std;

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	QStringList pictures;
	for(int i=1; i<app.arguments().count(); i++)
		if(!app.arguments().at(i).startsWith('-'))
			pictures << app.arguments().at(i);
	if(pictures.count()<1)
		pictures << "/usr/src/repos/qt3d/demos/pageflip/qqpage1.png";
	if(pictures.count()<2)
		pictures << "/usr/src/repos/qt3d/demos/pageflip/qqpage2.png";
	if(pictures.count()<3)
		pictures << "/usr/src/repos/qt3d/demos/pageflip/qqpage3.png";
	if(pictures.count()<4)
		pictures << "/usr/src/repos/qt3d/demos/pageflip/qqpage4.png";
	if(pictures.count()<5)
		pictures << "/usr/src/repos/qt3d/demos/pageflip/gradient.png";

	PageFlipView view(pictures);
	if (QApplication::arguments().contains(QLatin1String("-blend")))
		view.setBlend(true);
	if (QApplication::arguments().contains(QLatin1String("-vertical")))
		view.setVertical(true);
	if (QApplication::arguments().contains(QLatin1String("-maximize")))
		view.showMaximized();
	else if (QApplication::arguments().contains(QLatin1String("-fullscreen")))
		view.showFullScreen();
	else
		view.show();
	view.start(true);
	return app.exec();
}
