/****************************************************************************
 * Based on the Qt3D pageflip demo:
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in the Qt package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
****************************************************************************/

#include "PageFlipView"
#include <QDesktopWidget>
#include <QIOStream>

using namespace std;

PageFlipView::PageFlipView(QStringList const &images, QWidget *parent)
	: QGLWidget(parent),_images(images),_timer(0)
{
	posn = 0.0f;
	blend = false;
	_vertical = false;

	colors[0] = QColor(0, 192, 192, 255);
	colors[1] = QColor(192, 0, 0, 255);
	colors[2] = QColor(192, 192, 0, 255);
	colors[3] = QColor(128, 128, 0, 255);
	colorIndex = 0;

#if !defined(QT_OPENGL_ES_1)
	effect = new PageFlipGradientEffect();
#endif
}

PageFlipView::~PageFlipView()
{
#if !defined(QT_OPENGL_ES_1)
	delete effect;
#endif
}

void PageFlipView::start(bool stopAfterFlip, int speed)
{
	_stopAfterFlip=stopAfterFlip;
	if(_timer)
		delete _timer;
	_timer=new QTimer(this);
	connect(_timer, SIGNAL(timeout()), this, SLOT(animate()));
	_timer->start(speed);
}

void PageFlipView::stop()
{
	if(_timer) {
		delete _timer;
		_timer=0;
	}
}

void PageFlipView::resizeGL(int width, int height)
{
	glViewport(0, 0, width, height);
}

void PageFlipView::initializeGL()
{
	QGLPainter painter(this);

	//QSize size = rect().size();
	//int width = size.width() / 3;
	//int height = (int)(width * 1.414f);

	QImage img1(_images[0]);
	QImage img2(_images[1]);
	QImage img3(_images[2]);
	QImage img4(_images[3]);
	QImage gradient(_images[4]);

	pageSize = img1.size();
	// Make sure it doesn't get too big to display
	if(_vertical) {
		if(pageSize.width()>QApplication::desktop()->width()-50)
			img1=img1.scaledToWidth(QApplication::desktop()->width()-50, Qt::SmoothTransformation);
		if(img1.height()>QApplication::desktop()->height()/2-50)
			img1=img1.scaledToHeight(QApplication::desktop()->height()/2-50, Qt::SmoothTransformation);
	} else {
		if(pageSize.width()>QApplication::desktop()->width()/2-50)
			img1=img1.scaledToWidth(QApplication::desktop()->width()/2-50, Qt::SmoothTransformation);
		if(img1.height()>QApplication::desktop()->height()-80)
			img1=img1.scaledToHeight(QApplication::desktop()->height()-80, Qt::SmoothTransformation);
	}

	pageSize = img1.size();
	int width = pageSize.width();
	int height = pageSize.height();
	pageSize = QSize(width, height);

	// Make sure they're all the right size...
	if(img1.size() != pageSize)
		img1=img1.scaled(pageSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);
	if(img2.size() != pageSize)
		img2=img2.scaled(pageSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);
	if(img3.size() != pageSize)
		img3=img3.scaled(pageSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);
	if(img4.size() != pageSize)
		img4=img4.scaled(pageSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);
	if(gradient.width() != width)
		gradient=gradient.scaledToWidth(width, Qt::SmoothTransformation);

	textures[0].setImage(img1);
	textures[1].setImage(img2);
	textures[2].setImage(img3);
	textures[3].setImage(img4);

	gradientTexture.setImage(gradient);

	if (painter.hasOpenGLFeature(QOpenGLFunctions::BlendColor))
		painter.glBlendColor(0, 0, 0, 0);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	if (painter.hasOpenGLFeature(QOpenGLFunctions::BlendEquation))
		painter.glBlendEquation(GL_FUNC_ADD);
	else if (painter.hasOpenGLFeature(QOpenGLFunctions::BlendEquationSeparate))
		painter.glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);

	glEnable(GL_BLEND);

	if (_vertical)
		pageFlipMath.setStartCorner(PageFlipMath::VerticalBottomRight);
	else
		pageFlipMath.setStartCorner(PageFlipMath::BottomRight);
}

void PageFlipView::paintGL()
{
	QGLPainter painter(this);

	QRect rect = this->rect();
	int midx = rect.width() / 2;
	int topy = (rect.height() - pageSize.height()) / 2;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	QMatrix4x4 projm;
	projm.ortho(rect);
	painter.projectionMatrix() = projm;
	painter.modelViewMatrix().setToIdentity();

#if !defined(QT_OPENGL_ES_1)
	if (_vertical) {
		pageRect2 = QRect(QPoint(midx - pageSize.width() / 2, topy), pageSize);
		pageRect1 = QRect(QPoint(pageRect2.x() - pageSize.width(), topy), pageSize);
	} else {
		pageRect1 = QRect(QPoint(midx - pageSize.width(), topy), pageSize);
		pageRect2 = QRect(QPoint(midx, topy), pageSize);
	}
#else
	pageRect1 = QRect(QPoint(-pageSize.width(), 0), pageSize);
	pageRect2 = QRect(QPoint(0, 0), pageSize);
#endif
	pageFlipMath.setPageRect(pageRect2);
	pageFlipMath.setShowPageReverse(false);
	pageFlipMath.compute(posn);

	QGLAttributeValue positions
		(2, GL_FLOAT, pageFlipMath.stride(), pageFlipMath.vertexArray());
	QGLAttributeValue texCoords
		(2, GL_FLOAT, pageFlipMath.stride(), pageFlipMath.vertexArray() + 2);
	QGLAttributeValue gradientCoords
		(1, GL_FLOAT, pageFlipMath.stride(), pageFlipMath.vertexArray() + 4);

#if defined(QT_OPENGL_ES_1)
	painter.setStandardEffect(QGL::FlatReplaceTexture2D);
#else
	painter.setUserEffect(effect);
#endif
	painter.setColor(colors[colorIndex]);
	painter.glActiveTexture(GL_TEXTURE0);
	textures[colorIndex].bind();
	painter.glActiveTexture(GL_TEXTURE1);
	gradientTexture.bind();
	painter.setVertexAttribute(QGL::Position, positions);
	painter.setVertexAttribute(QGL::TextureCoord0, texCoords);
	painter.setVertexAttribute(QGL::CustomVertex0, gradientCoords);
	setAlphaValue(1.0f);
	painter.update();
	pageFlipMath.drawPage(0);

	painter.setColor(colors[(colorIndex + 1) % 4]);
	painter.glActiveTexture(GL_TEXTURE0);
	textures[(colorIndex + 1) % 4].bind();
	setAlphaValue(1.0f);
	painter.update();
	pageFlipMath.drawPage(1);

	painter.setColor(colors[(colorIndex + 2) % 4]);
	if (!pageFlipMath.showPageReverse())
		textures[(colorIndex + 2) % 4].bind();
	if (blend)
		setAlphaValue(0.75f);
	else
		setAlphaValue(1.0f);
	painter.update();
	pageFlipMath.drawPage(2);

	painter.setColor(colors[(colorIndex + 3) % 4]);
	textures[(colorIndex + 3) % 4].bind();
	setAlphaValue(1.0f);
	painter.update();
	pageFlipMath.drawPage(3);

	glBindTexture(GL_TEXTURE_2D, 0);
	painter.glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);

	painter.setStandardEffect(QGL::FlatColor);
	painter.setVertexAttribute(QGL::Position, positions);
	painter.setVertexAttribute(QGL::TextureCoord0, texCoords);
	painter.setVertexAttribute(QGL::CustomVertex0, gradientCoords);
	painter.setColor(QColor(0, 0, 0, 255));
	painter.update();
	pageFlipMath.drawOutline(2);
}

void PageFlipView::mousePressEvent(QMouseEvent *e)
{
	int x = e->x();
	int y = e->y();
	bool changed = true;
	if (_vertical) {
		if (x >= pageRect2.x() && x < (pageRect2.x() + 20) &&
				y >= pageRect2.y() && y < (pageRect2.y() + 20))
			pageFlipMath.setStartCorner(PageFlipMath::VerticalTopLeft);
		else if (x >= pageRect2.x() && x < (pageRect2.x() + 20) &&
					y >= (pageRect2.bottom() - 20) && y <= pageRect2.bottom())
			pageFlipMath.setStartCorner(PageFlipMath::VerticalBottomLeft);
		else if (x >= (pageRect2.right() - 20) && x <= pageRect2.right() &&
					y >= pageRect2.y() && y < (pageRect2.y() + 20))
			pageFlipMath.setStartCorner(PageFlipMath::VerticalTopRight);
		else if (x >= (pageRect2.right() - 20) && x <= pageRect2.right() &&
					y >= (pageRect2.bottom() - 20) && y <= pageRect2.bottom())
			pageFlipMath.setStartCorner(PageFlipMath::VerticalBottomRight);
		else
			changed = false;
	} else {
		if (x >= pageRect1.x() && x < (pageRect1.x() + 20) &&
				y >= pageRect1.y() && y < (pageRect1.y() + 20))
			pageFlipMath.setStartCorner(PageFlipMath::TopLeft);
		else if (x >= pageRect1.x() && x < (pageRect1.x() + 20) &&
					y >= (pageRect1.bottom() - 20) && y <= pageRect1.bottom())
			pageFlipMath.setStartCorner(PageFlipMath::BottomLeft);
		else if (x >= pageRect2.x() && x < (pageRect2.x() + 20) &&
					y >= pageRect2.y() && y < (pageRect2.y() + 20))
			pageFlipMath.setStartCorner(PageFlipMath::TopLeftOnePage);
		else if (x >= pageRect2.x() && x < (pageRect2.x() + 20) &&
					y >= (pageRect2.bottom() - 20) && y <= pageRect2.bottom())
			pageFlipMath.setStartCorner(PageFlipMath::BottomLeftOnePage);
		else if (x >= (pageRect2.right() - 20) && x <= pageRect2.right() &&
					y >= pageRect2.y() && y < (pageRect2.y() + 20))
			pageFlipMath.setStartCorner(PageFlipMath::TopRight);
		else if (x >= (pageRect2.right() - 20) && x <= pageRect2.right() &&
					y >= (pageRect2.bottom() - 20) && y <= pageRect2.bottom())
			pageFlipMath.setStartCorner(PageFlipMath::BottomRight);
		else
			changed = false;
	}
	if (changed)
		posn = 0.0f;
	QGLWidget::mousePressEvent(e);
	if(!_timer)
		start(true);
}

void PageFlipView::animate()
{
	posn += 0.04f;
	if (posn >= 1.0f) {
		posn = 0.0f;
		colorIndex = (colorIndex + 2) % 4;
	}
	updateGL();
	if(posn == 0.0f) {
		emit pageFlipped();
		if(_stopAfterFlip)
			stop();
	}
}

void PageFlipView::setAlphaValue(GLfloat value)
{
#if !defined(QT_OPENGL_ES_1)
	effect->setAlphaValue(value);
#else
	Q_UNUSED(value);
#endif
}

#if !defined(QT_OPENGL_ES_1)

static char const gradientVertexShader[] =
	"attribute highp vec4 qgl_Vertex;\n"
	"attribute highp vec4 qgl_TexCoord0;\n"
	"attribute highp float qgl_Custom0;\n"
	"uniform mediump mat4 qgl_ModelViewProjectionMatrix;\n"
	"varying highp vec4 qTexCoord0;\n"
	"varying highp float qGradCtrl;\n"
	"void main(void)\n"
	"{\n"
	"	gl_Position = qgl_ModelViewProjectionMatrix * qgl_Vertex;\n"
	"	qTexCoord0 = qgl_TexCoord0;\n"
	"	qGradCtrl = qgl_Custom0;\n"
	"}\n";

static char const gradientFragmentShader[] =
	"uniform sampler2D qgl_Texture0;\n"
	"uniform sampler2D qgl_Texture1;\n"
	"uniform mediump float alphaValue;\n"
	"varying highp vec4 qTexCoord0;\n"
	"varying highp float qGradCtrl;\n"
	"void main(void)\n"
	"{\n"
	"	mediump vec4 col = texture2D(qgl_Texture0, qTexCoord0.st);\n"
	"	mediump vec4 gradcol = texture2D(qgl_Texture1, vec2(qGradCtrl, qTexCoord0.t));\n"
	"	gl_FragColor = vec4((col * gradcol).xyz, alphaValue);\n"
	"}\n";

PageFlipGradientEffect::PageFlipGradientEffect()
{
	setVertexShader(gradientVertexShader);
	setFragmentShader(gradientFragmentShader);
}

PageFlipGradientEffect::~PageFlipGradientEffect()
{
}

void PageFlipGradientEffect::setAlphaValue(GLfloat value)
{
	program()->setUniformValue("alphaValue", value);
}

#endif // !QT_OPENGL_ES_1
