#include "Base64"

static char const encodeTable[64] =
{
	0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,
	0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50,
	0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58,
	0x59, 0x5A, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66,
	0x67, 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E,
	0x6F, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76,
	0x77, 0x78, 0x79, 0x7A, 0x30, 0x31, 0x32, 0x33,
	0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x2B, 0x2F
};

static char const decodeTable[128] =
{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x3F,
	0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B,
	0x3C, 0x3D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
	0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E,
	0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16,
	0x17, 0x18, 0x19, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20,
	0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
	0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30,
	0x31, 0x32, 0x33, 0x00, 0x00, 0x00, 0x00, 0x00
};

QByteArray Base64::decode(QString const & encoded)
{
	if (encoded.isEmpty()) return QByteArray();

	unsigned int len = encoded.length();
	QByteArray in = encoded.toAscii();

	char const * const data = in.data();

	unsigned int final = 0; // RFC 2045: final length does not count invalid chars in encoded string

	for (unsigned int idx=0; idx<len; ++idx)
	{
		uchar ch = data[idx];
		if ( (ch < 123) && ((ch == 65) || (decodeTable[ch] != 0)) )
			in.data()[final++] = decodeTable[ch];
	}

	if ( final < 1 ) return QByteArray();

	// base64 uses 4-byte blocks, but input may have exceeding bytes!
	unsigned int rest = final%4;
	final -= rest;

	len = final/4;

	switch (rest)
	{
	case 2: final = len*3 + 1; break; // we can get another byte out of 2 6-bit chars
	case 3: final = len*3 + 2; break; // we can get 2 bytes out of 3 6-bit chars
	default: final = len*3;
	}

	QByteArray out(final, '\0');

	unsigned int outIdx = 0; // index for the next decoded byte
	unsigned int inIdx = 0; // index for the next encoded byte

	// 4-byte to 3-byte conversion
	for (unsigned int idx=0; idx<len; ++idx)
	{
		out.data()[outIdx++] = (((in.at(inIdx) << 2) & 252) | ((in.at(inIdx+1) >> 4) & 003));
		out.data()[outIdx++] = (((in.at(inIdx+1) << 4) & 240) | ((in.at(inIdx+2) >> 2) & 017));
		out.data()[outIdx++] = (((in.at(inIdx+2) << 6) & 192) | (in.at(inIdx+3) & 077));
		inIdx += 4;
	}

	switch (rest)
	{
	case 2:
		out.data()[outIdx] = ((in.at(inIdx) << 2) & 252) | ((in.at(inIdx+1) >> 4) & 03); // get one byte out of two 6-bit chars
		break;
	case 3:
		out.data()[outIdx++] = ((in.at(inIdx) << 2) & 252) | ((in.at(inIdx+1) >> 4) & 03); // get one byte out of two 6-bit chars
		out.data()[outIdx] = ((in.at(inIdx+1) << 4) & 240) | ((in.at(inIdx+2) >> 2) & 017) ; // get next byte
	}

	return out;
}

QBitArray Base64::decode(const QString& encoded, unsigned int size)
{
	if (size == 0) return QBitArray();
	QByteArray ba = decode(encoded);

	unsigned int sz = ba.size()*8;
	if (sz == 0) return QBitArray();
	if (size < sz) sz = size;

	QBitArray bits(sz);

	unsigned int idx = 0, currentByte = 0;

	for (unsigned int i=0; i<sz; ++i)
	{
		if (idx == 8)
		{
			idx = 0;
			currentByte++;
		}

		bits.setBit(i, ba.at(currentByte) & 1<<(7-idx));
		idx++;
	}

	return bits;
}

QString Base64::encode(const QByteArray& decoded, bool limitLines)
{
	unsigned int len = decoded.size();

	if (len == 0) return QString();

	unsigned int idxIn = 0; // index of next decoded byte
	unsigned int idxOut = 0; // index for next encoded byte
	char const * const data = decoded.data(); // convenience pointer to decoded data

	unsigned int const rest = len%3;
	unsigned int final = (len < 3) ? 4 : (rest == 0) ? (len/3)*4 : (len/3)*4 + 4; // final output size

	if (limitLines)
		final += (len%76 == 0) ? (final/76 - 1)*2 : (final/76)*2;

	QByteArray out(final, '\0');

	len = (len-rest)/3;
	unsigned int charsOnLine = 0;

	// decoding + 3-byte to 4-byte conversion
	for (unsigned int idx=0; idx<len; ++idx)
	{
		if (limitLines && (charsOnLine > 0) && (charsOnLine%76 == 0))
		{ out.data()[idxOut++] = '\r'; out.data()[idxOut++] = '\n'; charsOnLine = 0; }

		out.data()[idxOut++] = encodeTable[(data[idxIn] >> 2) & 077];
		out.data()[idxOut++] = encodeTable[(data[idxIn+1] >> 4) & 017 | (data[idxIn] << 4) & 077];
		out.data()[idxOut++] = encodeTable[(data[idxIn+2] >> 6) & 003 | (data[idxIn+1] << 2) & 077];
		out.data()[idxOut++] = encodeTable[data[idxIn+2] & 077];
		idxIn += 3;
		charsOnLine += 4;
	}

	switch (rest)
	{
	case 1:
		// need to encode one last byte
		// add 4 0-bits to the right, encode two chars and append two '=' padding chars to output
		if (limitLines && (charsOnLine%76 == 0))
		{ out.data()[idxOut++] = '\r'; out.data()[idxOut++] = '\n'; }

		out.data()[idxOut++] = encodeTable[(data[idxIn] >> 2) & 077]; // encode first 6 bits
		out.data()[idxOut++] = encodeTable[(data[idxIn] << 4) & 077]; // encode next 6 bits (2 significant + 4 0-bits!)
		// padding
		out.data()[idxOut++] = '=';
		out.data()[idxOut] = '=';
		break;
	case 2:
		// need to encode two last bytes
		// add 2 0-bits to the right, encode three chars and append one '=' padding char to output
		if (limitLines && (charsOnLine%76 == 0))
		{ out.data()[idxOut++] = '\n'; }

		out.data()[idxOut++] = encodeTable[(data[idxIn] >> 2) & 077]; // encode first 6 bits
		out.data()[idxOut++] = encodeTable[(data[idxIn+1] >> 4) & 017 | (data[idxIn] << 4) & 077]; // encode next 6 bits
		out.data()[idxOut++] = encodeTable[(data[idxIn+1] << 2) & 077]; // last 6 bits (4 significant + 2 0-bits)
		// padding
		out.data()[idxOut] = '=';
	}

	return QString(out);
}

QString Base64::encode(const QBitArray& decoded, bool limitLines)
{
	unsigned int sz = decoded.size();
	unsigned int rest = sz%8;
	unsigned int bsz = (rest == 0) ? sz/8 : (sz/8)+1;
	unsigned int currByte = 0, idx = 0;

	QByteArray ba(bsz, '\0');

	for (unsigned int i=0; i<sz; i+=8)
	{
		ba.data()[currByte] = 0;

		for (idx=0; idx<8; ++idx)
		{
			if (i+idx == sz)
				break;

			if (decoded.at(i+idx))
				ba.data()[currByte] |= 1<<(7-idx);
		}

		currByte++;
	}

	return encode(ba,limitLines);
}
