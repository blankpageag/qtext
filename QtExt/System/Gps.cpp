#include "Gps"
#include <QStringList>
#include <QDebug>
#include <QDir>
#include "Coordinates"

Gps::Gps(QObject *parent, QString const &device):QObject(parent) {
	if(device.isNull()) {
		QDir dev("/dev");
		QStringList gpsDevs;
#if defined(Q_OS_LINUX) || defined(Q_OS_ANDROID)
		gpsDevs=dev.entryList(QStringList() << "ttyUSB*", QDir::System);
#elif defined(Q_OS_MAC)
		gpsDevs=dev.entryList(QStringList() << "cu.PL2303-*", QDir::System);
#else
#warning GPS is not fully supported on your OS. Please add information about your OS device nodes.
#endif
		foreach(QString const &d, gpsDevs) {
			_gps=new SerialPort(dev.absoluteFilePath(d));
			if(_gps->open(QFile::ReadOnly, B4800))
				break;
			else
				::destroy(_gps);
		}
		if(!_gps) {
			qDebug() << "GPS open failed";
			return;
		}
	} else {
		_gps=new SerialPort(device);
		if(!_gps->open(QFile::ReadOnly, B4800)) {
			qDebug() << "GPS open failed";
			destroy(_gps);
			return;
		}
	}
	connect(_gps, SIGNAL(readyRead()), this, SLOT(gpsData()));
}

Gps::~Gps() {
	destroy(_gps);
}

bool Gps::works() const {
	return _gps;
}

void Gps::gpsData() {
	QString s=_gps->readLine().trimmed();
	if(!s.startsWith("$GP")) // Either corrupted or not GPS data
		return;
	QString checksum=s.section('*', 1);
	QStringList data=s.section('*', 0, 0).split(',');
	QString const &cmd=data[0];
	if(cmd=="$GPGGA") {
		// Essential fix data, providing 3D location and accuracy data
		// $GPGGA,183530.000,4713.6562,N,00832.9736,E,1,05,2.8,611.4,M,48.0,M,,0000
		// 183530.000 Fix taken at 18:35:30.000 UTC
		// 4713.6562,N Latitude 47° 13.6562' N
		// 00832.9736,E Longitude 8° 32.9736' E
		// 1 Fix quality:
		// 	0 invalid
		// 	1 GPS fix (SPS)
		// 	2 DGPS fix
		// 	3 PPS fix
		// 	4 Real Time Kinematic
		// 	5 Float RTK
		// 	6 estimated (dead reckoning) (v2.3)
		// 	7 manual input mode
		// 	8 simulation mode
		// 05 Number of satellites being tracked
		// 2.8 Horizontal dilution of position
		// 611.4,M Altitude, Meters above mean sea level
		// 48.0,M Height of geoid (mean sea level) above WGS84 ellipsoid
		// (empty field) time in seconds since last DGPS update
		// 0000 DGPS station ID number
		qDebug() << data;
		QString time=data[1].left(2) + ":" + data[1].mid(2, 2) + ":" + data[1].mid(4, 2);
		if(data[6].toInt() == 0)
			// Let's ignore invalid entries
			return;
		// Skip some more invalid entries...
		if(data[2].isEmpty() || data[4].isEmpty())
			return;
		_currentLatitudeDegrees=(data[2].section('.', 0, 0).toInt())/100;
		double lat_minsec=data[2].toDouble()-100*_currentLatitudeDegrees;
		_currentLatitudeMinutes=lat_minsec;
		_currentLatitudeSeconds=(lat_minsec-_currentLatitudeMinutes)*60.0;
		if(data[3] == "S")
			_currentLatitudeDegrees = -_currentLatitudeDegrees;
		_currentLongitudeDegrees=(data[4].section('.', 0, 0).toInt())/100;
		double lon_minsec=data[4].toDouble()-100*_currentLongitudeDegrees;
		_currentLongitudeMinutes=lon_minsec;
		_currentLongitudeSeconds=(lon_minsec-_currentLongitudeMinutes)*60.0;
		if(data[5] == "W")
			_currentLongitudeDegrees = -_currentLongitudeDegrees;

		double latitude=coordinatesToDecimal(_currentLatitudeDegrees, _currentLatitudeMinutes, _currentLatitudeSeconds);
		double longitude=coordinatesToDecimal(_currentLongitudeDegrees, _currentLongitudeMinutes, _currentLongitudeSeconds);

		uint8_t zone=0;
		QPair<double,double> UTM=coordinatesToUTM(latitude, longitude, zone);

#ifdef DEBUG_GPS
		qDebug() << "Location at " << time << ":" << endl
		         << "	" << _currentLatitudeDegrees << "°" << _currentLatitudeMinutes << "'" << _currentLatitudeSeconds << "\"" << data[3] << endl
		         << "	" << "Decimal: " << latitude << endl
		         << "	" << "UTM: " << static_cast<int>(UTM.first) << endl
		         << "	" << _currentLongitudeDegrees << "°" << _currentLongitudeMinutes << "'" << _currentLongitudeSeconds << "\"" << data[3] << endl
		         << "	" << "Decimal: " << longitude << endl
		         << "	" << "UTM: " << static_cast<int>(UTM.second) << endl
		         << "	" << "Altitude: " << data[9] << data[10];
#endif

		_currentUTMX=UTM.second;
		_currentUTMY=UTM.first;
		_currentUTMZone=zone;
		_currentAltitude=data[9].toDouble();
		_last3DFix=::time(0);
		emit positionUpdated(_currentLatitudeDegrees, _currentLatitudeMinutes, _currentLatitudeSeconds, _currentLongitudeDegrees, _currentLongitudeMinutes, _currentLongitudeSeconds);
		emit UTMPositionUpdated(UTM.second, UTM.first, zone);
	} else if (cmd=="$GPGSA") {
		// GPS DOP and active satellites. This sentence provides details
		// on the nature of the fix. It includes the numbers of
		// satelites being used in the current solution and the DOP.
		// DOP (dilution of precision) is an indication of the effect
		// of satellite geometry on the accuracy of the fix. It is a
		// unitless number where smaller is better. For 3D fixes using
		// 4 satellites a 1.0 would be considered a perfect number,
		// however for overdetermined solutions it is possible to see
		// numbers below 1.0.
		// $GPGSA,A,3,20,23,11,13,32,,,,,,,,5.5,2.8,4.8
		// Some receivers might put satellite PRNs in random locations,
		// such as
		// $GPGSA,A,3,,,20,23,,11,13,,32,,,,5.5,2.8,4.8
		// A Auto selection of 2D or 3D fix (M = manual)
		// 3 Fix type
		// 	1 No fix
		// 	2 2D fix
		// 	3 3D fix
		// 20,23,11,13,32 PRNs of satellites being tracked (space for 12)
		// 5.5 PDOP (dilution of precision)
		// 2.8 HDOP (horizontal dilution of precision)
		// 4.8 VDOP (vertical dilution of precision)
	} else if(cmd=="$GPVTG") {
		// Velocity made good.
		// $GPVTG,131.35,T,,M,0.60,N,1.1,K
		// 131.35,T True track made good (degrees)
		// ,M Magnetic track made good
		// 0.60,N Ground speed, knots
		// 1.1,K Ground speed, kilometers per hour
#ifdef DEBUG_GPS
		qDebug() << "Current velocity: " << data[7] << "km/h";
#endif
	} else if(cmd=="$GPRMC") {
		// Essential GPS PVT (Position, Velocity, Time)
		// RMC == Recommended minimum
		// $GPRMC,183825.000,A,4713.6473,N,00832.9808,E,1.04,113.34,131208,,
		// 183825.000	Fix taken at 18:38:25.000 UTC
		// A		Status
		// 			A Active
		// 			V Void
		// 4713.6473,N	47° 13.6473' North
		// 00832.9808,E	8° 32.9808' East
		// 1.04		Speed over the ground in knots
		// 113.34	Track angle in degrees
		// 131208	Date (December 13, 2008)
		// Empty fields	Magnetic variation (format e.g. 003.1,W)
		if(data[3].isEmpty())
			return;
		QString time=data[1].left(2) + ":" + data[1].mid(2, 2) + ":" + data[1].mid(4, 2);
		_currentLatitudeDegrees=(data[3].section('.', 0, 0).toInt())/100;
		double lat_minsec=data[3].toDouble()-100*_currentLatitudeDegrees;
		_currentLatitudeMinutes=lat_minsec;
		_currentLatitudeSeconds=(lat_minsec-_currentLatitudeMinutes)*60.0;
		if(data[4] == "S")
			_currentLatitudeDegrees = -_currentLatitudeDegrees;
		_currentLongitudeDegrees=(data[5].section('.', 0, 0).toInt())/100;
		double lon_minsec=data[5].toDouble()-100*_currentLongitudeDegrees;
		_currentLongitudeMinutes=lon_minsec;
		_currentLongitudeSeconds=(lon_minsec-_currentLongitudeMinutes)*60.0;
		if(data[6] == "W")
			_currentLongitudeDegrees = -_currentLongitudeDegrees;

		double latitude=coordinatesToDecimal(_currentLatitudeDegrees, _currentLatitudeMinutes, _currentLatitudeSeconds);
		double longitude=coordinatesToDecimal(_currentLongitudeDegrees, _currentLongitudeMinutes, _currentLongitudeSeconds);

		uint8_t zone=0;
		QPair<double,double> UTM=coordinatesToUTM(latitude, longitude, zone);

#ifdef DEBUG_GPS
		qDebug() << "Location at " << time << ":" << endl
		         << "	" << _currentLatitudeDegrees << "°" << _currentLatitudeMinutes << "'" << _currentLatitudeSeconds << "\"" << data[3] << endl
		         << "	" << "Decimal: " << latitude << endl
		         << "	" << "UTM: " << static_cast<int>(UTM.first) << endl
		         << "	" << _currentLongitudeDegrees << "°" << _currentLongitudeMinutes << "'" << _currentLongitudeSeconds << "\"" << data[3] << endl
		         << "	" << "Decimal: " << longitude << endl
		         << "	" << "UTM: " << static_cast<int>(UTM.second) << endl
		         << "	" << "Altitude: " << data[9] << data[10];
#endif

		_currentUTMX=UTM.second;
		_currentUTMY=UTM.first;
		_currentUTMZone=zone;
		_last2DFix=::time(0);
		emit positionUpdated(_currentLatitudeDegrees, _currentLatitudeMinutes, _currentLatitudeSeconds, _currentLongitudeDegrees, _currentLongitudeMinutes, _currentLongitudeSeconds);
		emit UTMPositionUpdated(UTM.second, UTM.first, zone);
	} else if(cmd=="$GPGSV") {
		// Satellites in View shows data about the satellites that
		// the unit might be able to find based on its viewing mask
		// and almanac data. It also shows current ability to track
		// this data. Note that one GSV sentence only can provide
		// data for up to 4 satellites and thus there may be a need
		// for 3 sentences to provide the full information. It is
		// reasonable for the GSV sentence to contain more
		// satellites than GGA might indicate since GSV may include
		// satellites that are not usedddd as part of the solution.
		// It is not a requirement that the GSV sentences all appear
		// in sequence. To avoid overloading the data bandwidth some
		// receivers may place the variousssssssss sentences in
		// totally different samples since each sentence identifies
		// which one it is.
		// The field called SNR (signal to noise ratio) in the NMEA
		// standard is often referred to as signal strength. SNR is
		// an indirect but more useful value than raw signal
		// strength. It can range from 0 to 99 and has units of dB
		// according to the NMEA standard, but the various
		// manufacturers send different ranges of numbers with
		// different starting numbers so the values themselves cannot
		// necessarily be used to evaluate different units. The range
		// of working values in a given GPS will usually show a
		// difference of about 25 to 35 between the lowest and
		// highest values, however 0 is a special case and may be
		// shown on satellites that are in view but not being
		// tracked.
		//
		// $GPGSV,3,1,09,23,77,188,31,20,65,073,34,13,50,212,35,32,40,078,
		// 3	Number of sentences for full data
		// 1	Sentence 1 of 3
		// 09	Number of satellites in view
		// For up to 4 satellites per sentence:
		// 23	Satellite PRN number
		// 77	Elevation, degrees
		// 188	Azimuth, degrees
		// 31	SNR - higher is better
	} else
		qDebug() << "unprocessed GPS data: " << endl << s;
}

void Gps::getUTMPosition(int &x, int &y, uint8_t &zone) const {
	x=_currentUTMX;
	y=_currentUTMY;
	zone=_currentUTMZone;
}
