#include "NetworkInterface"
#include "WiFiInterface"
#include "FD"
#include <QIOStream>

extern "C" {
#ifndef Q_WS_WIN32
#include <stdint.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#if !defined(Q_OS_IPHONE) && (!defined(Q_OS_DARWIN) || !defined(Q_WS_QPA))
#include <net/route.h>
#endif
#include <arpa/inet.h>
#endif
#include <errno.h>
#ifdef __linux__
#define WIRELESS_SUPPORTED
#define _LINUX_IF_H
#include <linux/types.h>
#include <linux/wireless.h>
#else
#ifdef _MSC_VER
#pragma message("Please implement wireless support for your OS (and fix your compiler to understand #warning)")
#else
#warning Please implement wireless support for your OS!
#endif
#endif
}

using namespace std;

NetworkInterface::NetworkInterface(char const * const name) {
	strncpy(_name, name, IFNAMSIZ);
	_interface = QNetworkInterface::interfaceFromName(_name);
	if(!_interface.isValid()) {
		// Try to get the name from humanReadableName
		QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
		foreach(QNetworkInterface const &intf, interfaces) {
			if(intf.humanReadableName() == _name) {
				strncpy(_name, intf.name().toLatin1().data(), IFNAMSIZ);
				_interface=intf;
				break;
			}
		}
	}
}

NetworkInterface *NetworkInterface::get(QString const &name) {
	return NetworkInterface::get(name.toLatin1().data());
}

NetworkInterface *NetworkInterface::get(char const * const name) {
#ifdef WIRELESS_SUPPORTED
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	struct iwreq iwr;
	strncpy(iwr.ifr_name, name, IFNAMSIZ);
	bool wireless=!ioctl(fd, SIOCGIWNAME, &iwr);
	if(wireless)
		return new WiFiInterface(name);
#endif
	return new NetworkInterface(name);
}

QString NetworkInterface::macAddress() const {
	if(!_interface.isValid())
		return QString::null;
	return _interface.hardwareAddress();
}

QHostAddress NetworkInterface::ip(bool preferv6) const
{
	if(!_interface.isValid())
		return QHostAddress::Null;

	QList<QNetworkAddressEntry> const addressEntries = _interface.addressEntries();
	foreach(QNetworkAddressEntry const &addressEntry, addressEntries) {
		QHostAddress const address=addressEntry.ip();
		if((preferv6 && address.protocol() == QAbstractSocket::IPv6Protocol) ||
		   (!preferv6 && address.protocol() == QAbstractSocket::IPv4Protocol))
			return address;
	}
	// Preferred address wasn't found -- so return the first one there is... if any
	if(addressEntries.isEmpty())
		return QHostAddress();
	return addressEntries.first().ip();
}

QHostAddress NetworkInterface::netmask(bool preferv6) const
{
	if(!_interface.isValid())
		return QHostAddress::Null;

	QList<QNetworkAddressEntry> const addressEntries = _interface.addressEntries();
	foreach(QNetworkAddressEntry const &addressEntry, addressEntries) {
		QHostAddress const address=addressEntry.netmask();
		if((preferv6 && address.protocol() == QAbstractSocket::IPv6Protocol) ||
		   (!preferv6 && address.protocol() == QAbstractSocket::IPv4Protocol))
			return address;
	}
	// Preferred address wasn't found -- so return the first one there is... if any
	if(addressEntries.isEmpty())
		return QHostAddress();
	return addressEntries.first().netmask();
}

QHostAddress NetworkInterface::broadcast(bool preferv6) const
{
	if(!_interface.isValid())
		return QHostAddress::Null;

	QList<QNetworkAddressEntry> const addressEntries = _interface.addressEntries();
	foreach(QNetworkAddressEntry const &addressEntry, addressEntries) {
		QHostAddress const address=addressEntry.broadcast();
		if((preferv6 && address.protocol() == QAbstractSocket::IPv6Protocol) ||
		   (!preferv6 && address.protocol() == QAbstractSocket::IPv4Protocol))
			return address;
	}
	// Preferred address wasn't found -- so return the first one there is... if any
	if(addressEntries.isEmpty())
		return QHostAddress();
	return addressEntries.first().broadcast();
}

bool NetworkInterface::setIp(QHostAddress const &ip) const
{
#ifdef SIOCSIFADDR
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	struct ifreq ifr;
	strncpy(ifr.ifr_name, _name, IFNAMSIZ);
	switch(ip.protocol()) {
	case QAbstractSocket::IPv4Protocol: {
		struct sockaddr_in sin;
		sin.sin_family = AF_INET;
		sin.sin_port = 0;
		sin.sin_addr.s_addr = htonl(ip.toIPv4Address());
		memcpy(reinterpret_cast<char*>(&ifr.ifr_addr), reinterpret_cast<char*>(&sin), sizeof(struct sockaddr_in));
		break;
	}
	case QAbstractSocket::IPv6Protocol: {
		struct sockaddr_in6 sin;
		sin.sin6_family = AF_INET6;
		sin.sin6_port = 0;
		memcpy(sin.sin6_addr.s6_addr, ip.toIPv6Address().c, 16);
		sin.sin6_scope_id = ip.scopeId().toUInt();
		memcpy((&ifr.ifr_addr), &sin, sizeof(struct sockaddr_in6));
		break;
	}
	default:
		qDebug() << "Unsupported network protocol -- only IPv4 and IPv6 are supported." << endl
		         << "Please extend QtExt and send a patch.";
		return false;
	};
	bool success=!ioctl(fd, SIOCSIFADDR, &ifr);
	return success;
#else
#ifdef _MSC_VER
#pragma message("NetworkInterface::setIp isn't implemented for your OS")
#else
#warning NetworkInterface::setIp is not implemented for your OS
#endif
	return false;
#endif
}

bool NetworkInterface::setNetmask(QHostAddress const &ip) const
{
#ifdef SIOCSIFNETMASK
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	struct ifreq ifr;
	strncpy(ifr.ifr_name, _name, IFNAMSIZ);
	switch(ip.protocol()) {
	case QAbstractSocket::IPv4Protocol: {
		struct sockaddr_in sin;
		sin.sin_family = AF_INET;
		sin.sin_port = 0;
		sin.sin_addr.s_addr = htonl(ip.toIPv4Address());
		memcpy(reinterpret_cast<char*>(&ifr.ifr_addr), reinterpret_cast<char*>(&sin), sizeof(struct sockaddr_in));
		break;
	}
	case QAbstractSocket::IPv6Protocol: {
		struct sockaddr_in6 sin;
		sin.sin6_family = AF_INET6;
		sin.sin6_port = 0;
		memcpy(sin.sin6_addr.s6_addr, ip.toIPv6Address().c, 16);
		sin.sin6_scope_id = ip.scopeId().toUInt();
		memcpy((&ifr.ifr_addr), &sin, sizeof(struct sockaddr_in6));
		break;
	}
	default:
		qDebug() << "Unsupported network protocol -- only IPv4 and IPv6 are supported." << endl
		         << "Please extend QtExt and send a patch.";
		return false;
	};
	bool success=!ioctl(fd, SIOCSIFNETMASK, &ifr);
	return success;
#else
#ifdef _MSC_VER
#pragma message("NetworkInterface::setNetmask isn't implemented for your OS")
#else
#warning NetworkInterface::setNetmask is not implemented for your OS
#endif
	return false;
#endif
}

bool NetworkInterface::setBroadcast(QHostAddress const &ip) const
{
#ifdef SIOCSIFBRDADDR
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	struct ifreq ifr;
	strncpy(ifr.ifr_name, _name, IFNAMSIZ);
	switch(ip.protocol()) {
	case QAbstractSocket::IPv4Protocol: {
		struct sockaddr_in sin;
		sin.sin_family = AF_INET;
		sin.sin_port = 0;
		sin.sin_addr.s_addr = htonl(ip.toIPv4Address());
		memcpy(reinterpret_cast<char*>(&ifr.ifr_addr), reinterpret_cast<char*>(&sin), sizeof(struct sockaddr_in));
		break;
	}
	case QAbstractSocket::IPv6Protocol: {
		struct sockaddr_in6 sin;
		sin.sin6_family = AF_INET6;
		sin.sin6_port = 0;
		memcpy(sin.sin6_addr.s6_addr, ip.toIPv6Address().c, 16);
		sin.sin6_scope_id = ip.scopeId().toUInt();
		memcpy((&ifr.ifr_addr), &sin, sizeof(struct sockaddr_in6));
		break;
	}
	default:
		qDebug() << "Unsupported network protocol -- only IPv4 and IPv6 are supported." << endl
		         << "Please extend QtExt and send a patch.";
		return false;
	};
	bool success=!ioctl(fd, SIOCSIFBRDADDR, &ifr);
	return success;
#else
#ifdef _MSC_VER
#pragma message("NetworkInterface::setBroadcast isn't implemented for your OS")
#else
#warning NetworkInterface::setBroadcast is not implemented for your OS.
#endif
	return false;
#endif
}

bool NetworkInterface::setName(QString const &name) {
#ifdef SIOCSIFNAME
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	struct ifreq ifr;
	strncpy(ifr.ifr_name, _name, IFNAMSIZ);
#ifdef __FreeBSD__
	strncpy(ifr.ifr_data, name.toLatin1().data(), IFNAMSIZ);
#else
	strncpy(ifr.ifr_newname, name.toLatin1().data(), IFNAMSIZ);
#endif
	bool success=!ioctl(fd, SIOCSIFNAME, &ifr);
	if(success)
		strncpy(_name, name.toLatin1().data(), IFNAMSIZ);
	return success;
#else
#ifdef _MSC_VER
#pragma message("Please implement interface name changes for your OS! (Or switch to Linux)")
#else
#warning Please implement interface name changes for your OS!
#endif
	return false;
#endif
}

bool NetworkInterface::setFlag(short flag) const {
#ifdef SIOCSIFFLAGS
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	struct ifreq ifr;
	strncpy(ifr.ifr_name, _name, IFNAMSIZ);
	ifr.ifr_flags = flags() | flag;
	bool success=!ioctl(fd, SIOCSIFFLAGS, &ifr);
	return success;
#else
#ifdef _MSC_VER
#pragma message("Implement NetworkInterface::setFlag for your OS")
#else
#warning Implement NetworkInterface::setFlag for your OS
#endif
	return false;
#endif
}

bool NetworkInterface::clearFlag(short flag) const {
#ifdef SIOCSIFFLAGS
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	struct ifreq ifr;
	strncpy(ifr.ifr_name, _name, IFNAMSIZ);
	ifr.ifr_flags = flags() & ~flag;
	bool success=!ioctl(fd, SIOCSIFFLAGS, &ifr);
	return success;
#else
#ifdef _MSC_VER
#pragma message("Implement NetworkInterface::clearFlag for your OS (or get Linux)")
#else
#warning Implement NetworkInterface::clearFlag for your OS
#endif
	return false;
#endif
}

short NetworkInterface::flags() const {
#ifdef SIOCGIFFLAGS
	// We don't just use QNetworkInterface::flags() on every OS because
	// SIOCGIFFLAGS provides more information
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	struct ifreq ifr;
	strncpy(ifr.ifr_name, _name, IFNAMSIZ);
	ioctl(fd, SIOCGIFFLAGS, &ifr);
	return ifr.ifr_flags;
#else
	return _interface.flags();
#endif
}

bool NetworkInterface::isWireless() const {
#ifndef WIRELESS_SUPPORTED
	return false;
#else
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	struct iwreq iwr;
	strncpy(iwr.ifr_name, _name, IFNAMSIZ);
	bool success=!ioctl(fd, SIOCGIWNAME, &iwr);
	return success;
#endif
}

QString NetworkInterface::wirelessType() const {
#ifndef WIRELESS_SUPPORTED
	return QString::null;
#else
	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return QString::null;
	struct iwreq iwr;
	strncpy(iwr.ifr_name, _name, IFNAMSIZ);
	bool success=!ioctl(fd, SIOCGIWNAME, &iwr);
	if(success)
		return reinterpret_cast<char*>(&iwr.u.nwid);
	return QString::null;
#endif
}

bool NetworkInterface::exists() const {
	return _interface.isValid();
}

bool NetworkInterface::setHostname(QString const &hostname) const
{
#ifdef Q_OS_WIN
#ifdef _MSC_VER
#pragma message("Windoze doesn't seem to know about hostnames?")
#else
#warning Windoze does not seem to know about hostnames?
#endif
	return false;
#elif defined(ANDROID)
#warning Android does not seem to have sethostname()
	return false;
#else
	return sethostname(hostname.toLatin1().data(), hostname.length()) == 0;
#endif
}

bool NetworkInterface::addRoute(QHostAddress const &destination, QHostAddress const &netmask, QHostAddress const &gateway, int extraFlags) const
{
#if defined(SIOCADDRT) && !defined(__FreeBSD__) // FreeBSD has different semantics on SIOCADDRT
	sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
#if AF_INET != 0 // At least on Linux, this is already 0, so we can save a few cycles
	addr.sin_family = AF_INET;
#endif

	rtentry route;
	memset(&route, 0, sizeof(route));
	route.rt_dev = const_cast<char*>(_name);
	route.rt_flags = RTF_UP|extraFlags;
	if(!gateway.isNull())
		route.rt_flags |= RTF_GATEWAY;
	if(netmask == QHostAddress("255.255.255.255"))
		route.rt_flags |= RTF_HOST;
	route.rt_metric = 0;
	addr.sin_addr.s_addr = htonl(destination.toIPv4Address());
	memcpy(&route.rt_dst, &addr, sizeof(addr));
	addr.sin_addr.s_addr = htonl(netmask.toIPv4Address());
	memcpy(&route.rt_genmask, &addr, sizeof(addr));
	addr.sin_addr.s_addr = htonl(gateway.toIPv4Address());
	memcpy(&route.rt_gateway, &addr, sizeof(addr));

	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	if(ioctl(fd, SIOCADDRT, &route) != 0)
		return false;
	return true;
#else
#ifdef _MSC_VER
#pragma message("Please implement addRoute for your OS (or switch to Linux)")
#else
#warning Please implement addRoute for your OS
#endif
	return false;
#endif
}

bool NetworkInterface::delRoute(QHostAddress const &destination, QHostAddress const &netmask, QHostAddress const &gateway, int extraFlags) const
{
#if defined(SIOCDELRT) && !defined(__FreeBSD__) // FreeBSD has different semantics on SIOCDELRT
	sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
#if AF_INET != 0 // At least on Linux, this is already 0, so we can save a few cycles
	addr.sin_family = AF_INET;
#endif

	rtentry route;
	memset(&route, 0, sizeof(route));
	route.rt_dev = const_cast<char*>(_name);
	route.rt_flags = RTF_UP|extraFlags;
	if(!gateway.isNull())
		route.rt_flags |= RTF_GATEWAY;
	if(netmask == QHostAddress("255.255.255.255"))
		route.rt_flags |= RTF_HOST;
	route.rt_metric = 0;
	addr.sin_addr.s_addr = htonl(destination.toIPv4Address());
	memcpy(&route.rt_dst, &addr, sizeof(addr));
	addr.sin_addr.s_addr = htonl(netmask.toIPv4Address());
	memcpy(&route.rt_genmask, &addr, sizeof(addr));
	addr.sin_addr.s_addr = htonl(gateway.toIPv4Address());
	memcpy(&route.rt_gateway, &addr, sizeof(addr));

	FD fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0)
		return false;
	if(ioctl(fd, SIOCDELRT, &route) != 0)
		return false;
	return true;
#else
#ifdef _MSC_VER
#pragma message("Please implement delRoute for your OS (or switch to Linux)")
#else
#warning Please implement delRoute for your OS
#endif
	return false;
#endif
}
