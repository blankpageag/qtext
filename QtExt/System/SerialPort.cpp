#include "SerialPort"

extern "C" {
#include <fcntl.h>
#include <unistd.h>
}

#include <QDebug>

bool SerialPort::open(QFile::OpenMode m, int speed, uint8_t databits, SerialPort::Parity parity, uint8_t stopbits) {
	if(_notifier)
		delete _notifier;

	mode_t mode=O_NOCTTY;
	if(m & QFile::ReadWrite)
		mode |= O_RDWR;
	else if(m & QFile::WriteOnly)
		mode |= O_WRONLY;
	else
		mode |= O_RDONLY;
	if(m & QIODevice::Append)
		mode |= O_APPEND;
	if(m & QIODevice::Truncate)
		mode |= O_TRUNC;
	int fd=::open(QFile::encodeName(fileName()), mode);
	if(fd<0)
		return false;

	int db;
	switch(databits) {
	case 5:
		db=5;
		break;
	case 6:
		db=6;
		break;
	case 7:
		db=7;
		break;
	default:
		db=8;
	}
	termios tio;
	tcgetattr(fd, &tio);
	tio.c_cflag = speed | CRTSCTS | db | (stopbits == 2 ? CSTOPB : 0) | parity | CLOCAL | CREAD;
	tio.c_iflag = IGNPAR;
	tio.c_oflag = 0;
	tio.c_lflag = 0;
	tio.c_cc[VMIN] = 1;
	tio.c_cc[VTIME] = 0;
	tcflush(fd, TCIFLUSH);
	tcsetattr(fd, TCSANOW, &tio);

	bool b=QFile::open(fd, m);

	_notifier=new QSocketNotifier(fd, QSocketNotifier::Read, this);
	connect(_notifier, SIGNAL(activated(int)), this, SIGNAL(readyRead()));
	if(!b)
		qDebug() << "QFile::open failed";
	return b;
}

SerialPort::~SerialPort() {
	if(_notifier) {
		int fd=_notifier->socket();
		delete _notifier;
		QFile::close();
		::close(fd);
	}
}
