#include "Coordinates"
#include <cmath>

double coordinatesToDecimal(int degrees, int minutes, double const &seconds)
{
	return degrees + minutes/60.0 + seconds/3600.0;
}

void decimalToCoordinates(double decimal, int &degrees, int &minutes, double &seconds) {
	degrees = static_cast<int>(decimal);
	decimal -= degrees;
	decimal *= 60.0;
	minutes = static_cast<int>(decimal);
	decimal -= minutes;
	seconds = decimal*60;
}

// UTM conversion as provided below is a C++ port of the JavaScript
// code found at http://home.hiwaay.net/~taylorc/toolbox/geography/geoutm.html


// Constants for ellipsoid model, WGS84
static double const pi = 3.141592653589793238462643383279502884197169399375105820974944;
static double const sm_a = 6378137.0;
static double const sm_b = 6356752.314;
static double const sm_EccSquared = 6.69437999013e-03;
static double const UTMScaleFactor = 0.9996;

static double const n = (sm_a - sm_b) / (sm_a + sm_b);
static double const alpha = ((sm_a + sm_b) / 2.0) * (1.0 + (pow(n, 2.0)/4.0) + (pow(n, 4.0)/64.0));

static double degToRad(double const &deg) {
	return deg/180.0*pi;
}

static double radToDeg(double const &rad) {
	return rad/pi*180.0;
}

/**
 * Compute the ellipsoidal distance from the equator to a point at a
 * given latitude.
 *
 * @param phi Latitude of the point in radians
 * @return Ellipsoidal distance of the point from the equator, in meters
 */
static double arcLengthOfMeridian(double phi) {
	double const beta = (-3.0*n/2.0) + (9.0*pow(n, 3.0)/16.0)+(-3.0*pow(n, 5.0)/32.0);
	double const gamma = (15.0*pow(n, 2.0)/16.0) + (-15.0*pow(n, 4.0)/32.0);
	double const delta = (-35.0*pow(n, 3.0)/48.0)+(105.0*pow(n, 5.0)/256.0);
	double const epsilon = (315.0*pow(n, 4.0)/512.0);
	return alpha * (phi + (beta*sin(2.0*phi))+(gamma*sin(4.0*phi))+(delta*sin(6.0*phi))+(epsilon*sin(8.0*phi)));
}

/**
 * Determine the central meridian for the given UTM zone
 * @param zone UTM zone, range 1-60
 * @return central meridian for the given UTM zone in radians, or zero if the
 * zone parameter is out of range.
 * Range of the central meridian is the radian equivalent of [-177,+177].
 */
static double UTMCentralMeridian(uint8_t zone) {
	return degToRad(-183.0+(zone*6.0));
}


static double const footpointBeta = (3.0*n/2.0)+(-27.0*pow(n, 3.0)/32.0)+(269.0*pow(n, 5.0)/512.0);
static double const footpointGamma = (21.0*pow(n, 2.0)/16.0)+(-55.0*pow(n, 4.0)/32.0);
static double const footpointDelta = (151.0*pow(n, 3.0)/96.0)+(-417.0*pow(n, 5.0)/128.0);
static double const footpointEpsilon = (1097.0*pow(n, 4.0)/512.0);

/**
 * Compute the footpoint latitude for use in converting transverse Mercator
 * ooordinates to ellipsoidal coordinates.
 * @param y UTM northing coordinate in meters
 * @return footpoint latitude in radians
 */
static double footpointLatitude(double y) {
	double const y_=y/alpha;
	return y_ + (footpointBeta*sin(2.0*y_)) +
	       (footpointGamma*sin(4.0*y_)) +
	       (footpointDelta*sin(6.0*y_)) +
	       (footpointEpsilon*sin(8.0*y_));
}

static double const ep2=(pow(sm_a, 2.0)-pow(sm_b, 2.0))/pow(sm_b, 2.0);

/**
 * Convert a latitude/longitude pair to X and Y coordinates in the
 * Transverse Mercator projection. Note that Transverse Mercator is not
 * the same as UTM; a scale factor is required to convert between them.
 * @param latitude Latitude in radians
 * @param longitude Longitude in radians
 * @param centralMeridian central meridian
 */
static QPair<double,double> coordinatesToXY(double latitude, double longitude, double centralMeridian) {
	double const nu2=ep2*pow(cos(latitude), 2.0);
	double const N=pow(sm_a, 2.0)/(sm_b*sqrt(1+nu2));
	double const t=tan(latitude);
	double const t2=pow(t, 2);
	double const tmp=pow(t2, 3)-pow(t, 6.0);
	double const l=longitude-centralMeridian;
	double const l3coef=1.0-t2+nu2;
	double const l4coef=5.0-t2+9*nu2+4.0*pow(nu2, 2);
	double const l5coef=5.0-18.0*t2+pow(t2,2)+14.0*nu2;
	double const l6coef=61.0-58.0*t2+pow(t2,2)+270.0*nu2-330.0*t2*nu2;
	double const l7coef=61.0-479.0*t2+179.0*pow(t2,2)-pow(t2,3);
	double const l8coef=1385.0-3111.0*t2+543.0*pow(t2,2)-pow(t2,3);

	double const easting=N*cos(latitude)*l +
		(N/6.0*pow(cos(latitude), 3.0)*l3coef*pow(l, 3.0)) +
		(N/120.0*pow(cos(latitude), 5.0)*l5coef*pow(l, 5.0)) +
		(N/5040.0*pow(cos(latitude), 7.0)*l7coef*pow(l, 7.0));

	double const northing=arcLengthOfMeridian(latitude) +
		(t/2.0*N*pow(cos(latitude), 2.0)*pow(l, 2.0)) +
		(t/24.0*N*pow(cos(latitude), 4.0)*l4coef*pow(l, 4.0)) +
		(t/720.0*N*pow(cos(latitude), 6.0)*l6coef*pow(l, 6.0)) +
		(t/40320.0*N*pow(cos(latitude), 8.0)*l8coef*pow(l, 8.0));
	return QPair<double,double>(easting, northing);
}

/**
 * Convert x and y coordinates in the Transverse Mercator projection to a
 * latitude/longitude pair. Note that Transverse Mercator is not the same
 * as UTM; a scale factor is required to convert between them.
 * @param x Easting in meters
 * @param y Northing in meters
 * @param centralMeridian Longitude of the central meridian in radians
 * @return Latitude and longitude in radians
 */
static QPair<double,double> XYToCoordinates(double x, double y, double centralMeridian) {
	double const phif=footpointLatitude(y);
	double const cf=cos(phif);
	double const nuf2=ep2*pow(cf, 2.0);
	double const Nf=pow(sm_a, 2.0)/(sm_b*sqrt(1+nuf2));
	double Nfpow=Nf;
	double const tf=tan(phif);
	double const tf2=tf*tf;
	double const tf4=tf2*tf2;
	double const x1frac=1.0/(Nfpow*cf);
	Nfpow *= Nf; // Now Nf^2
	double const x2frac=tf/(2.0*Nfpow);
	Nfpow *= Nf; // Nf^3
	double const x3frac=1.0/(6.0*Nfpow*cf);
	Nfpow *= Nf; // Nf^4
	double const x4frac=tf/(24.0*Nfpow);
	Nfpow *= Nf; // Nf^5
	double const x5frac=1.0/(120.0*Nfpow*cf);
	Nfpow *= Nf; // Nf^6
	double const x6frac=tf/(720.0*Nfpow);
	Nfpow *= Nf; // Nf^7
	double const x7frac=1.0/(5040.0*Nfpow*cf);
	Nfpow *= Nf; // Nf^8
	double const x8frac = tf/(40320.0*Nfpow);
	double const x2poly=-1.0-nuf2;
	double const x3poly=-1.0-2*tf2-nuf2;
	double const x4poly=5.0+3.0*tf2+6.0*nuf2-6.0*tf2*nuf2-3.0*(nuf2*nuf2)-9.0*tf2*(nuf2*nuf2);
	double const x5poly=5.0+28.0*tf2+24.0*tf4+6.0*nuf2+8.0*tf2*nuf2;
	double const x6poly=-61.0-90.0*tf2-45.0*tf4-107.0*nuf2+162.0*tf2*nuf2;
	double const x7poly=-61.0-662.0*tf2-1320.0*tf4-720.0*(tf4*tf2);
	double const x8poly=1385.0+3633.0*tf2+4095.0*tf4+1575*(tf4*tf2);
	double const latitude=phif+x2frac*x2poly*(x*x) +
		x4frac*x4poly*pow(x, 4.0) +
		x6frac*x6poly*pow(x, 6.0) +
		x8frac*x8poly*pow(x, 8.0);
	double const longitude=centralMeridian+x1frac*x +
		x3frac*x3poly*pow(x, 3.0) +
		x5frac*x5poly*pow(x, 5.0) +
		x7frac*x7poly*pow(x, 7.0);
	return QPair<double,double>(latitude,longitude);
}

QPair<double,double> coordinatesToUTM(double latitude, double longitude, uint8_t &zone) {
	if(zone<1 || zone>60)
		zone=static_cast<uint8_t>((longitude+180.0)/6)+1;
	latitude=degToRad(latitude);
	longitude=degToRad(longitude);
	QPair<double,double> XY=coordinatesToXY(latitude, longitude, UTMCentralMeridian(zone));
	QPair<double,double> result=QPair<double,double>(XY.first*UTMScaleFactor+500000.0, XY.second*UTMScaleFactor);
	if(result.second<0.0)
		result.second += 10000000.0;
	return result;
}

QPair<double,double> UTMToCoordinates(double x, double y, uint8_t zone, bool southhemi) {
	x -= 500000.0;
	x /= UTMScaleFactor;
	if(southhemi)
		y -= 10000000.0;
	y /= UTMScaleFactor;
	double const cmeridian=UTMCentralMeridian(zone);
	QPair<double,double> coor=XYToCoordinates(x, y, cmeridian);
	return QPair<double,double>(radToDeg(coor.first), radToDeg(coor.second));
}
