#include "WiFiInterface"
#include <QProcess>
#include <cassert>

WiFiInterface::WiFiInterface(char const * const name):NetworkInterface(name) {
}

QList<WiFiNetwork*> WiFiInterface::scan() {
#ifdef __linux__
	up();
	QProcess iw;
	iw.start("/usr/sbin/iw", QStringList() << "dev" << _name << "scan");
	iw.waitForFinished();
	QList<WiFiNetwork*> result;
	WiFiNetwork *current=0;
	while(iw.canReadLine()) {
		QString l=iw.readLine();
		if(l.startsWith("	") || l.startsWith(" ")) {
			l=l.trimmed();
			if(l.startsWith("SSID:")) {
				assert(current);
				current->ssid=l.section(": ", 1, -1);
			} else if(l.startsWith("freq:")) {
				assert(current);
				current->freq=l.section(": ", 1, -1).toUInt();
			} else if(l.startsWith("signal:")) {
				current->signalStrength=l.section(": ", 1, 1).section(' ', 0, 0).toFloat();
			} else if(l.startsWith("Supported rates:") || l.startsWith("Extended supported rates:")) {
				QString r=l.section(": ", 1, -1);
				foreach(QString rate, r.split(' '))
					current->rates << rate.section('*', 0, 0).toFloat();
			} else if(l.startsWith("WPA:")) {
				current->encryption = WiFiNetwork::WPA;
			} else if(l.startsWith("WEP:")) {
				current->encryption = WiFiNetwork::WEP;
			}
		} else {
			if(current)
				result << current;
			current=new WiFiNetwork;
			current->dev=this;
			current->BSS=l.section(' ', 1, 1);
			current->encryption = WiFiNetwork::Open;
		}
	}
	if(current)
		result << current;
	foreach(WiFiNetwork* c, result)
		qDebug() << "Found network:" << c->ssid;
	return result;
#else
#warning Please implement WiFiInterface::scan() for your OS
	return QList<WiFiNetwork*>();
#endif
}

QString WiFiInterface::currentNetwork() const {
#ifdef __linux__
	QProcess iw;
	iw.start("/usr/sbin/iw", QStringList() << "dev" << _name << "link");
	iw.waitForFinished();
	while(iw.canReadLine()) {
		QString l=iw.readLine();
		if(l.startsWith("	") || l.startsWith(" ")) {
			l=l.trimmed();
			if(l.startsWith("SSID:"))
				return l.section(": ", 1, -1);
		}
	}
#else
#warning Please implement WiFiInterface::currentInterface() for your OS
#endif
	return QString::null;
}

void WiFiInterface::connect(QString const &ssid) {
#ifdef __linux__
	QProcess iw;
	iw.start("/usr/sbin/iw", QStringList() << "dev" << _name << "connect" << ssid);
	iw.waitForFinished();
#else
#warning Please implement WiFiInterface::connect() for your OS
#endif
}
