#include <QDebug>
#include <FD>
#include "Netlink"

#include <cstdlib>

#define NLMSG_TAIL(nmsg) reinterpret_cast<rtattr*>((reinterpret_cast<char*>(nmsg))+NLMSG_ALIGN((nmsg)->nlmsg_len))

NetlinkRequest::NetlinkRequest(int len, int flags, int type):NLRequest() {
	memset(this, 0, sizeof(NLRequest));
	n.nlmsg_len = NLMSG_LENGTH(len);
	n.nlmsg_flags = flags;
	n.nlmsg_type = type;
}

bool NetlinkRequest::addAttribute(int type, QByteArray const &data) {
	int const len = RTA_LENGTH(data.size()+1);
	int const alignedLen = RTA_ALIGN(len);
	if((int)NLMSG_ALIGN(n.nlmsg_len) + alignedLen > 4096)
		return false;

	rtattr * const rta = NLMSG_TAIL(&n);
	rta->rta_type = type;
	rta->rta_len = len;
	memcpy(RTA_DATA(rta), data.constData(), data.size());
	n.nlmsg_len = NLMSG_ALIGN(n.nlmsg_len) + alignedLen;
	return true;
}

NetlinkSocket::NetlinkSocket(int type, int subscriptions):_fd(-1) {
	_fd=socket(AF_NETLINK, SOCK_RAW, type);
	int sndbuf = 32768, rcvbuf = 32768;
	setsockopt(_fd, SOL_SOCKET, SO_SNDBUF, &sndbuf, sizeof(sndbuf));
	setsockopt(_fd, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf));
	memset(&_local, 0, sizeof(_local));
	_local.nl_family = AF_NETLINK;
	_local.nl_groups = subscriptions;
	socklen_t addr_len = sizeof(_local);
	if(bind(_fd, reinterpret_cast<sockaddr*>(&_local), sizeof(_local))<0)
		goto error;

	if(getsockname(_fd, reinterpret_cast<sockaddr*>(&_local), &addr_len)<0)
		goto error;

	if(addr_len != sizeof(_local))
		goto error;

	if(_local.nl_family != AF_NETLINK)
		goto error;

	return;

error:
	close(_fd);
	_fd=-1;
}

nlmsghdr *NetlinkSocket::send(NetlinkRequest &req) {
	int status;
	static unsigned seq = 0;
	nlmsghdr *h;
	bool answerRequested;
	sockaddr_nl nladdr;
	iovec iov;
	memset(&iov, 0, sizeof(iov));
	iov.iov_base = (void*)&req;
	iov.iov_len = req.n.nlmsg_len;
	msghdr msg;
	memset(&msg, 0, sizeof(msg));
	msg.msg_name = &nladdr;
	msg.msg_namelen = sizeof(nladdr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	char buf[16384];
	memset(&nladdr, 0, sizeof(nladdr));
	nladdr.nl_family = AF_NETLINK;
	pid_t peer=0;
	nladdr.nl_pid = peer;
	nladdr.nl_groups = 0; // groups
	req.n.nlmsg_seq = ++seq;
	answerRequested=req.n.nlmsg_flags & NLM_F_ACK;
	status = sendmsg(_fd, &msg, 0);
	if(status<0)
		return 0;
	memset(buf, 0, sizeof(buf));
	iov.iov_base = buf;
	forever {
		iov.iov_len = sizeof(buf);
		status = recvmsg(_fd, &msg, 0);
		if(status<0)
			continue;
		if(status==0)
			return 0; //EOF
		if(msg.msg_namelen != sizeof(nladdr))
			return 0; //corrupted data
		for(h=reinterpret_cast<nlmsghdr*>(buf); static_cast<unsigned>(status)>=sizeof(*h); ) {
			int err;
			int len = h->nlmsg_len;
			int l = len - sizeof(*h);
			if(l<0 || len>status) {
				if(msg.msg_flags & MSG_TRUNC)
					return 0; // Truncated message
				return 0; // Malformed message
			}
			if(nladdr.nl_pid != static_cast<unsigned>(peer) ||
			   h->nlmsg_pid != _local.nl_pid ||
			   h->nlmsg_seq != seq) {
				qDebug() << "Netlink received junk";
				// junk...
				status -= NLMSG_ALIGN(len);
				h = reinterpret_cast<nlmsghdr*>(reinterpret_cast<char*>(h)+NLMSG_ALIGN(len));
				continue;
			}
			if(h->nlmsg_type == NLMSG_ERROR) {
				qDebug() << "Netlink Error";
				nlmsgerr *msgerr = reinterpret_cast<nlmsgerr*>(NLMSG_DATA(h));
				if(static_cast<unsigned>(l)<sizeof(nlmsgerr))
					return 0; // error truncated
				else {
					errno = -msgerr->error;
					if(errno==0) {
						nlmsghdr *answer=0;
						if(answerRequested) {
							answer=reinterpret_cast<nlmsghdr*>(new char[h->nlmsg_len]);
							if(answer)
								memcpy(answer, h, h->nlmsg_len);
						}
						return answer;
					}
				}
				return 0;
			}
			if(answerRequested) {
				nlmsghdr *answer=reinterpret_cast<nlmsghdr*>(new char[h->nlmsg_len]);
				if(answer)
					memcpy(answer, h, h->nlmsg_len);
				return answer;
			}
			status -= NLMSG_ALIGN(len);
			h = reinterpret_cast<nlmsghdr*>(reinterpret_cast<char*>(h)+NLMSG_ALIGN(len));
		}
		if(msg.msg_flags & MSG_TRUNC)
			continue; // truncated message
		if(status) {
			return 0; // unknown remnant
		}
	}
}

QHash<int, rtattr*> Netlink::attributes(rtattr *attrs, socklen_t len) {
	QHash<int, rtattr*> ret;
	while(RTA_OK(attrs, len)) {
		ret.insert(attrs->rta_type, attrs);
		attrs=RTA_NEXT(attrs, len);
	}
	return ret;
}

uint32_t Netlink::mcastBitMap(uint32_t group) {
	if(group > 31)
		return -1; // Doesn't fit into a uint32_t...
	return group ? (1 << (group - 1)) : 0;
}

uint32_t GeNetlink::id(QByteArray const &name) {
	NetlinkRequest req(GENL_HDRLEN, NLM_F_REQUEST|NLM_F_ACK, GENL_ID_CTRL);

	genlmsghdr *ghdr = reinterpret_cast<genlmsghdr * const>(NLMSG_DATA(&req.n));
	ghdr->cmd = CTRL_CMD_GETFAMILY;
	req.addAttribute(CTRL_ATTR_FAMILY_NAME, name);

	NetlinkSocket sck(NETLINK_GENERIC, 0);
	nlmsghdr *answer=sck.send(req);
	if(!answer)
		return -1;

	ghdr = reinterpret_cast<genlmsghdr*>(NLMSG_DATA(answer));
	int len = answer->nlmsg_len - NLMSG_LENGTH(GENL_HDRLEN);
	rtattr *attrs;
	if(len<0) { // wrong message
		delete[] answer;
		return -1;
	}
	if(ghdr->cmd != CTRL_CMD_GETFAMILY &&
	   ghdr->cmd != CTRL_CMD_DELFAMILY &&
	   ghdr->cmd != CTRL_CMD_NEWFAMILY &&
	   ghdr->cmd != CTRL_CMD_NEWMCAST_GRP &&
	   ghdr->cmd != CTRL_CMD_DELMCAST_GRP) {
		delete[] answer;
		return -1; // unknown command
	}

	attrs = reinterpret_cast<rtattr*>(reinterpret_cast<char*>(ghdr) + GENL_HDRLEN);
	QHash<int, rtattr*> tb=attributes(attrs, len);

	if(tb.contains(CTRL_ATTR_FAMILY_ID))
		return *(reinterpret_cast<uint32_t*>(RTA_DATA(tb[CTRL_ATTR_FAMILY_ID])));

	return -1;
}


uint32_t GeNetlink::mcastGroupID(QByteArray const &channel, QByteArray const &groupName) {
	NetlinkRequest req(GENL_HDRLEN, NLM_F_REQUEST|NLM_F_ACK, GENL_ID_CTRL);

	genlmsghdr *ghdr = reinterpret_cast<genlmsghdr * const>(NLMSG_DATA(&req.n));
	ghdr->cmd = CTRL_CMD_GETFAMILY;
	req.addAttribute(CTRL_ATTR_FAMILY_NAME, channel);

	NetlinkSocket sck(NETLINK_GENERIC, 0);
	nlmsghdr *answer=sck.send(req);
	if(!answer)
		return -1;

	ghdr = reinterpret_cast<genlmsghdr*>(NLMSG_DATA(answer));
	int len = answer->nlmsg_len - NLMSG_LENGTH(GENL_HDRLEN);
	rtattr *attrs;
	if(len<0) { // wrong message
		delete[] answer;
		return -1;
	}
	if(ghdr->cmd != CTRL_CMD_GETFAMILY &&
	   ghdr->cmd != CTRL_CMD_DELFAMILY &&
	   ghdr->cmd != CTRL_CMD_NEWFAMILY &&
	   ghdr->cmd != CTRL_CMD_NEWMCAST_GRP &&
	   ghdr->cmd != CTRL_CMD_DELMCAST_GRP) {
		delete[] answer;
		return -1; // unknown command
	}

	attrs = reinterpret_cast<rtattr*>(reinterpret_cast<char*>(ghdr) + GENL_HDRLEN);
	QHash<int, rtattr*> tb=attributes(attrs, len);
	if(tb.contains(CTRL_ATTR_MCAST_GROUPS)) {
		rtattr *mcastGroups=reinterpret_cast<rtattr*>(RTA_DATA(tb[CTRL_ATTR_MCAST_GROUPS]));
		len = RTA_PAYLOAD(tb[CTRL_ATTR_MCAST_GROUPS]);
		while(RTA_OK(mcastGroups, len)) {
			QHash<int,rtattr*> tb3;
			rtattr *mcastGroup=reinterpret_cast<rtattr*>(RTA_DATA(mcastGroups));
			int subLen=RTA_PAYLOAD(mcastGroups);
			while(RTA_OK(mcastGroup, subLen)) {
				tb3.insert(mcastGroup->rta_type, mcastGroup);
				mcastGroup = RTA_NEXT(mcastGroup, subLen);
			}
			if(tb3.contains(CTRL_ATTR_MCAST_GRP_NAME) && tb3.contains(CTRL_ATTR_MCAST_GRP_ID)) {
				char *name=static_cast<char*>(RTA_DATA(tb3[CTRL_ATTR_MCAST_GRP_NAME]));
				if(groupName==name || !groupName.size()) {
					uint32_t mcGroupId=*((uint32_t*)RTA_DATA(tb3[CTRL_ATTR_MCAST_GRP_ID]));
					delete[] answer;
					return mcGroupId;
				}
			}
			mcastGroups = RTA_NEXT(mcastGroups, len);
		}
	}
	delete[] answer;
	return -1;
}

NetlinkMessage::NetlinkMessage(int fd) {
	nlmsghdr tmp;
	int size=recv(fd, &tmp, sizeof(tmp), MSG_PEEK);
	if(size!=sizeof(tmp))
		return;
	_type=tmp.nlmsg_type;
	char buffer[tmp.nlmsg_len];
	size=recv(fd, &buffer, tmp.nlmsg_len, MSG_DONTWAIT);
	if(size!=tmp.nlmsg_len)
		return;
	nlmsghdr const * const hdr=reinterpret_cast<nlmsghdr const * const>(&buffer);
	ifinfomsg const * const ifi=reinterpret_cast<ifinfomsg const * const>(NLMSG_DATA(hdr));
	memcpy(&_ifInfo, ifi, sizeof(ifinfomsg));
	if(!ifi)
		return;
	int attr_len=IFLA_PAYLOAD(hdr);
	rtattr const * attr=reinterpret_cast<rtattr const *>(IFLA_RTA(ifi));
	if(!attr)
		return;
	while(RTA_OK(attr, attr_len)) {
		_attributes.insert(attr->rta_type, QByteArray(reinterpret_cast<char const * const>(RTA_DATA(attr)), RTA_PAYLOAD(attr)));
		attr=RTA_NEXT(attr, attr_len);
	}
}
