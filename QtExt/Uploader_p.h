#ifndef UPLOADER_P_H
#define UPLOADER_P_H

#include <QHash>
#include <QIODevice>
#include <QLinkedList>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QSet>
#include <QUrl>
#include <QVariant>
#include "UploaderObserver"

//
//  W A R N I N G
//  -------------
//
// This file is not part of the QtExt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

class UploaderDataProvider
{
public:
	virtual ~UploaderDataProvider() {}
	virtual bool atEnd() const =0;
	virtual qint64 read(char *data, qint64 maxSize) =0;
	virtual qint64 size() const =0;

protected:
	UploaderDataProvider() {}
};


class UploaderByteArrayProvider : public UploaderDataProvider
{
public:
	UploaderByteArrayProvider(QByteArray const& data) :
		UploaderDataProvider(),
		_data(data),
		_position(0)
	{
	}

	bool atEnd() const
	{
		return _position >= _data.length();
	}

	qint64 read(char *data, qint64 maxSize)
	{
		qint64 remainingBytes = maxSize;
		if(_position + maxSize > _data.length()) {
			remainingBytes = _data.length() - _position;
		}
		memcpy(data, _data.constData(), remainingBytes);
		_position += remainingBytes;
		return remainingBytes;
	}

	qint64 size() const
	{
		return _data.length() - _position;
	}

private:
	QByteArray _data;
	int _position;
};


class UploaderIODeviceProvider : public UploaderDataProvider
{
public:
	UploaderIODeviceProvider(QIODevice *device) :
		UploaderDataProvider(),
		_device(device)
	{
	}

	bool atEnd() const
	{
		return _device->atEnd();
	}

	qint64 read(char *data, qint64 maxSize)
	{
		return _device->read(data, maxSize);
	}

	qint64 size() const
	{
		return _device->size();
	}

private:
	QIODevice *_device;
};




class UploaderPrivate : public QIODevice
{
	Q_OBJECT

public:
	UploaderPrivate(QUrl const& url,
					QHash<QString, QVariant> const& fields,
					QHash<QString, QPair<QString, QIODevice*> > const& devices) :
		_request(url),
		_accessManager(0),
		_privateAccessManager(0),
		_boundary(QString("--"+QString::number(qrand(), 10)).toAscii()),
		_fields(fields),
		_devices(devices)
	{
	}

	~UploaderPrivate()
	{
		qDeleteAll(_dataProviders);
	}

	bool atEnd() const
	{
		return _dataProviders.isEmpty();
	}

	bool open(OpenMode mode)
	{
		QHashIterator<QString, QPair<QString, QIODevice*> > it(_devices);
		while(it.hasNext()) {
			QIODevice *device = it.next().value().second;
			if(!device->isOpen()) {
				if(!device->open(QIODevice::ReadOnly)) {
					return false;
				}
			}
		}

		qDeleteAll(_dataProviders);
		QHashIterator<QString, QVariant> fieldIt(_fields);
		while(fieldIt.hasNext()) {
			QByteArray value;
			if(!fieldIt.hasPrevious()) {
				value += "--" + _boundary + "\r\n";
			}
			fieldIt.next();
			value += "Content-Disposition: form-data; name=\"" + fieldIt.key().toUtf8() + "\"\r\n\r\n";
			value += fieldIt.value().toByteArray();
			value += closingBoundary(!fieldIt.hasNext() && _devices.isEmpty());
			_dataProviders << new UploaderByteArrayProvider(value);
		}

		QHashIterator<QString, QPair<QString, QIODevice*> > fileIt(_devices);
		while(fileIt.hasNext()) {
			fileIt.next();
			QByteArray value = "Content-Disposition: form-data; name=\"" + fileIt.key().toUtf8() + "\"; filename=\"" + fileIt.value().first.toUtf8() + "\"\r\n";
			value += "Content-Type: application/octet-stream\r\n";
			value += "Content-Length: " + QByteArray::number(fileIt.value().second->size()) + "\r\n\r\n";
			_dataProviders << new UploaderByteArrayProvider(value);
			_dataProviders << new UploaderIODeviceProvider(fileIt.value().second);
			_dataProviders << new UploaderByteArrayProvider(closingBoundary(!fieldIt.hasNext()));
		}

		return QIODevice::open(mode);
	}

	qint64 readData(char *data, qint64 maxSize)
	{
		qint64 i = 0;
		while(i < maxSize-1 && !_dataProviders.isEmpty()) {
			UploaderDataProvider *provider = _dataProviders.first();
			i += provider->read(data + i, maxSize - i);
			if(provider->atEnd()) {
				delete provider;
				_dataProviders.removeFirst();
			}
		}
		return i;
	}

	qint64 writeData(const char *data, qint64 maxSize)
	{
		// This class is read-only
		return 0;
	}

	qint64 size() const
	{
		qint64 totalSize = 0;
		foreach(UploaderDataProvider *provider, _dataProviders) {
			totalSize += provider->size();
		}
		return totalSize;
	}

	QNetworkRequest _request;
	QNetworkAccessManager *_accessManager;
	QNetworkAccessManager *_privateAccessManager;
	QByteArray _boundary;
	QHash<QString, QVariant> _fields;
	QHash<QString, QPair<QString, QIODevice*> > _devices;
	QSet<UploaderObserver*> _observers;

public slots:
	void reportProgress(qint64 bytesSent, qint64 bytesTotal)
	{
		foreach(UploaderObserver *observer, _observers) {
			observer->uploadProgress(bytesSent, bytesTotal);
		}
	}

private:
	QByteArray closingBoundary(bool last) const
	{
		if(!last) {
			return "\r\n--" + _boundary + "\r\n";
		} else {
			return "\r\n--" + _boundary + "--\r\n";
		}
	}

private:
	QLinkedList<UploaderDataProvider*> _dataProviders;
};

#endif
