#include "FunctionDebug"
#include <QDebug>

int FunctionDebug::_indent=0;

FunctionDebug::FunctionDebug(QString const &f):_func(f) {
	qDebug() << QString(" ").repeated(_indent++) << "Entering " << f << endl;
}

FunctionDebug::~FunctionDebug() {
	qDebug() << QString(" ").repeated(--_indent) << "Leaving " << _func << endl;
}
