#ifndef _BASE64_ 
#define _BASE64_ 1

#include <QString>
#include <QByteArray>
#include <QBitArray>
#include <CompilerTools>

/** @class Base64 Base64 <Base64>
 * Base64 encoder and decoder.
 * Base64 encodes arbitrary data into the characters A-Z, a-z and 0-9, making
 * sure the content doesn't get modified on systems that modify the encoding of
 * messages they received.
 * It is commonly used for binary e-mail attachments.
 */
class QTEXT_EXPORT Base64
{
public:
	/** Decode a Base64 string.
	 * @param encoded Encoded string
	 * @return decoded data
	 */
	static QByteArray decode(QString const & encoded);
	/** Decode a Base64 string.
	 * @param encoded Encoded string
	 * @param size maximum size
	 * @return decoded data
	 */
	static QBitArray decode(QString const & encoded, unsigned int size);
	/** Encode arbitrary data into Base64.
	 * @param decoded data to be encoded
	 * @param limitLines If @c true, the output is limited to 72 characters per line
	 * @return Base64 encoded data
	 */
	static QString encode(QByteArray const & decoded, bool limitLines = false);
	/** Encode arbitrary data into Base64.
	 * @param decoded data to be encoded
	 * @param limitLines If @c true, the output is limited to 72 characters per line
	 * @return Base64 encoded data
	 */
	static QString encode(QBitArray const & decoded, bool limitLines = false);
};
#endif
