#include "MediaPlayer"
#include <QApplication>
#include <QPainter>
#include <QIOStream>

using namespace std;

static QString mplayer = MPLAYER;

MediaPlayer::MediaPlayer(bool hasVideo, QWidget *parent):QLabel(parent),_player(0),_playerCrashed(false),_hasVideo(hasVideo),_wd()
{
	if(_hasVideo) {
		setAutoFillBackground(true);
		QPalette p(palette());
		p.setBrush(QPalette::Window, Qt::black);
		setPalette(p);
	} else
		setFixedSize(0, 0);
}

void MediaPlayer::setMplayerPath(QString const &path)
{
	mplayer = path;
}

void MediaPlayer::paintEvent(QPaintEvent *e)
{
	if(_hasVideo) {
		if(!_player || _player->state() != QProcess::Running) {
			QLabel::paintEvent(e);
			return;
		} else {
			QPainter p(this);
			p.setBrush(Qt::black);
			p.drawRect(rect());
		}
	}
}

void MediaPlayer::playPause(bool play)
{
	if(!_player) {
		qApp->flush(); // make sure winId() is already valid
		_playerCrashed=false;
		_player=new QProcess(this);
		if(!_wd.isEmpty())
			_player->setWorkingDirectory(_wd);
		connect(_player, SIGNAL(readyReadStandardOutput()), this, SLOT(stdoutData()));
		connect(_player, SIGNAL(readyReadStandardError()), this, SLOT(stderrData()));
		connect(_player, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(playerStopped()));
		QStringList videoArgs, audioArgs;
		if(_hasVideo)
#ifdef Q_WS_X11
			videoArgs << "-vo" << "xv:ck=set,x11,sdl,gl2,gl" << "-colorkey" << "0";
#elif defined(Q_WS_QWS)
			videoArgs << "-vo" << "fbdev";
#elif defined(Q_WS_MACX)
			videoArgs << "-vo" << "quartz";
#elif defined(Q_WS_WIN32) || defined(Q_WS_WIN64)
			videoArgs << "-vo" << "xv:ck=set,x11,sdl,gl2,gl";
#else
#warning Please add the correct mplayer -vo option for your window system
			;
#endif
		else
			videoArgs << "-vo" << "null";

#if defined(Q_WS_X11) || defined(Q_WS_QWS)
		audioArgs << "-ao" << "sdl,oss,alsa";
#elif defined(Q_WS_MACX)
		audioArgs << "-ao" << "openal";
#elif defined(Q_WS_WIN32) || defined(Q_WS_WIN64)
		audioArgs << "-ao" << "dsound";
#else
#warning Please add the correct mplayer -ao option for your OS
#endif

		if(_startPos)
			videoArgs << "-ss" << QString::number(_startPos/1000.0);

		_player->start(mplayer, QStringList() << videoArgs << audioArgs << _extraArgs << "-slave" << "-v" << "-wid" << QString::number((uint64_t)winId()) << _media);
	} else {
		if(_player && _player->state() == QProcess::Running)
			_player->write("pause\n", 6);
	}
}

void MediaPlayer::rew()
{
	seek(-1.5);
}

void MediaPlayer::ff()
{
	seek(1.0, "+");
}

void MediaPlayer::seek(double position, QString const &direction)
{
	QString cmd="seek " + direction + QString::number(position) + "\n";
	if(_player && _player->state() == QProcess::Running) {
		_player->write(cmd.toLatin1().data(), cmd.length());
	}
}

void MediaPlayer::sendCommand(QString const &command)
{
	if(_player && _player->state() == QProcess::Running)
		_player->write((command + "\n").toLatin1().data(), command.length()+1);
}

void MediaPlayer::stop()
{
	if(_player && _player->state() == QProcess::Running)
		_player->write("quit\n", 5);
	else if(_player) {
		_player->deleteLater();
		_player=0;
	}
}

void MediaPlayer::stdoutData()
{
	_player->setReadChannel(QProcess::StandardOutput);
	while(_player->canReadLine()) {
		QString s(_player->readLine());
		if(s.contains("*** screenshot")) {
			QString ss=s.mid(s.indexOf("*** screenshot ")+15);
			ss=ss.section("'", 1, 1);
			emit screenshotTaken(ss);
		}
	}
}

void MediaPlayer::stderrData()
{
	_player->setReadChannel(QProcess::StandardError);
	while(_player->canReadLine()) {
		QString s(_player->readLine());
		if(s.contains("interrupted by signal 8") || s.contains("interrupted by signal 4"))
			_playerCrashed=true;
	}
}

void MediaPlayer::playerStopped()
{
	_player->deleteLater();
	_player=0;
	if(_playerCrashed) // Just in case mplayer crashed on startup...
		playPause(true);
	else if(_hasVideo)
		repaint();
	emit stopped();
}



MediaControls::MediaControls(QWidget *parent):QWidget(parent),_layout(this),_ffrewTimer(0)
{
	_play=new QPushButton(QIcon(":/QtExt/play.svg"), tr("Play/Pause"), this);
	_play->setCheckable(true);
	_layout.addWidget(_play);
	_rew=new QPushButton(QIcon(":/QtExt/rew.svg"), tr("Rewind"), this);
	_rew->setCheckable(true);
	_layout.addWidget(_rew);
	_ff=new QPushButton(QIcon(":/QtExt/ff.svg"), tr("Fast Forward"), this);
	_ff->setCheckable(true);
	_layout.addWidget(_ff);
	_stop=new QPushButton(QIcon(":/QtExt/stop.svg"), tr("Stop"), this);
	_layout.addWidget(_stop);
	connect(_play, SIGNAL(toggled(bool)), this, SLOT(playPauseClicked(bool)));
	connect(_rew, SIGNAL(toggled(bool)), this, SLOT(rewind(bool)));
	connect(_ff, SIGNAL(toggled(bool)), this, SLOT(fastforward(bool)));
	connect(_stop, SIGNAL(clicked()), this, SIGNAL(stop()));
	connect(_stop, SIGNAL(clicked()), this, SLOT(stopped()));
}

void MediaControls::stopped()
{
	_play->setChecked(false);
	_play->setDown(false);
}

void MediaControls::rewind(bool start)
{
	if(start && _ff->isChecked()) {
		_ff->setChecked(false);
		_ff->setDown(false);
	}
	if(_ffrewTimer) {
		_ffrewTimer->stop();
		_ffrewTimer->deleteLater();
		_ffrewTimer=0;
	}
	if(start) {
		_rew->setDown(true);
		emit rew();
		_ffrewTimer=new QTimer(this);
		connect(_ffrewTimer, SIGNAL(timeout()), this, SIGNAL(rew()));
		_ffrewTimer->setSingleShot(false);
		_ffrewTimer->start(200);
	}
}

void MediaControls::fastforward(bool start)
{
	if(start && _rew->isChecked()) {
		_rew->setChecked(false);
		_rew->setDown(false);
	}
	if(_ffrewTimer) {
		_ffrewTimer->stop();
		_ffrewTimer->deleteLater();
		_ffrewTimer=0;
	}
	if(start) {
		_ff->setDown(true);
		emit ff();
		_ffrewTimer=new QTimer(this);
		connect(_ffrewTimer, SIGNAL(timeout()), this, SIGNAL(ff()));
		_ffrewTimer->setSingleShot(false);
		_ffrewTimer->start(200);
	}
}

bool MediaControls::stopFfRew()
{
	if(_ff->isChecked() || _rew->isChecked()) {
		// Some users think "Play" is "stop ff/rew"
		// Let's give them what they expect.
		fastforward(false);
		rewind(false);
		return true;
	}
	return false;
}

void MediaControls::playPauseClicked(bool play)
{
	fastforward(false);
	rewind(false);
	_ff->setChecked(false);
	_ff->setDown(false);
	_rew->setChecked(false);
	_rew->setDown(false);
	_play->setChecked(play);
	_play->setDown(play);
	emit playPause(play);
}

MediaWidget::MediaWidget(bool hasVideo, QWidget *parent, bool autoplay, Qt::WindowFlags f):QWidget(parent,f),_layout(this),_autoplay(autoplay)
{
	_media=new MediaPlayer(hasVideo, this);
	_layout.addWidget(_media);
	_layout.setStretchFactor(_media, hasVideo ? 1000 : 0);
	_controls=new MediaControls(this);
	_layout.addWidget(_controls);
	_layout.setStretchFactor(_controls, hasVideo ? 0 : 1000);
	connect(_controls, SIGNAL(playPause(bool)), _media, SLOT(playPause(bool)));
	connect(_controls, SIGNAL(stop()), _media, SLOT(stop()));
	connect(_controls, SIGNAL(rew()), _media, SLOT(rew()));
	connect(_controls, SIGNAL(ff()), _media, SLOT(ff()));
	connect(_media, SIGNAL(stopped()), _controls, SLOT(stopped()));
	connect(_controls, SIGNAL(stop()), this, SIGNAL(stop()));
	connect(_media, SIGNAL(stopped()), this, SIGNAL(stop()));
	connect(_media, SIGNAL(screenshotTaken(QString const &)), this, SIGNAL(screenshotTaken(QString const &)));
}

void MediaWidget::play()
{
	emit playing();
	if(!_controls->stopFfRew())
		_media->playPause(true);
}

void MediaWidget::showEvent(QShowEvent *e)
{
	QWidget::showEvent(e);
	if(_autoplay)
		_controls->playPauseClicked(true);
}
