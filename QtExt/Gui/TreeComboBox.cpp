#include "TreeComboBox"
#include <QMouseEvent>
#include <QTreeView>
#include <QHeaderView>
#include <QIOStream>

using namespace std;

TreeComboBox::TreeComboBox(QWidget *parent):QComboBox(parent),_ignoreHide(false) {
	QTreeView *v=new QTreeView(this);
	v->header()->hide();
	setView(v);
	view()->viewport()->installEventFilter(this);
}

bool TreeComboBox::eventFilter(QObject *object, QEvent *event) {
	if(event->type() == QEvent::MouseButtonPress && object==view()->viewport()) {
		QMouseEvent const *me=static_cast<QMouseEvent*>(event);
		if(!view()->visualRect(view()->indexAt(me->pos())).contains(me->pos()))
			_ignoreHide=true;
	}
	return false;
}

void TreeComboBox::showPopup()
{
	setRootModelIndex(QModelIndex());
	QComboBox::showPopup();
}

void TreeComboBox::hidePopup()
{
	if(_ignoreHide)
		_ignoreHide=false;
	else {
		if(!view()->currentIndex().isValid())
			// might happen when changing the model...
			return;
		setRootModelIndex(view()->currentIndex().parent());
		setCurrentIndex(view()->currentIndex().row());
		QComboBox::hidePopup();
		emit popupClosed();
	}
}
