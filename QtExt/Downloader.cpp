#include "Downloader"
#include "DownloaderObserver"
#include <QNetworkReply>
#include <QFile>
#include <QDebug>

Downloader::Downloader(QByteArray &target):QEventLoop(0),_target(target),_reply(0), 
_storagePath(), 
_packageSize(-1),
_nextPackageIndex(_packageSize){
}
Downloader::Downloader(QByteArray &target, QString iStoragePath, int iPackages):QEventLoop(0),_target(target),_reply(0), 
_storagePath(iStoragePath), 
_packageSize(iPackages),
_nextPackageIndex(_packageSize)
{
}

void Downloader::downloadFinished() 
{
	if(_reply && !_reply->error()) 
	{
		_target=_reply->readAll();
		foreach( DownloaderObserver* o, _observers) 
		{
			o->storeDownload();
		}
	}
	else
	{
		qWarning() <<"QNetworkReply error in Downloader: "<<_reply->error();
	}
	quit();
}

// Fake Range support for legacy servers
static void truncate(QByteArray &content, QNetworkReply *reply, int start, int end) {
	if((start != 0 || end != -1) && !content.isEmpty()) {
		if(start<0) {
			start=content.size()-end;
			end=content.size()-1;
		} else if(end==-1)
			end=content.size()-1;
		if(reply->rawHeaderList().contains("Content-Range")) {
			// We may want to verify the Content-Range is actually the same
			// range we specified... But until we actually see a misbehaving
			// server, let's just assume the server is ok and do nothing
			return;
		}
		// The server did not send a Content-Range header - so either it doesn't
		// support the HTTP/1.1 Range header or it's not an HTTP server.
		// The requested data is a subset of what we got though - so let's extract
		// what we're interested in.
		if(start<0) {
			start=content.size()-end;
			end=content.size()-1;
		}
		if(start<0 || end>=content.size() || start>end)
			// Invalid range => invalid download
			content.clear();
		else
			content=content.mid(start, end-start+1);
	}
}

QByteArray Downloader::get(QUrl const &url, QNetworkAccessManager *nam, QObject *progressObject, char const * const progressSlot, int start, int end) {
	bool defaultNam=!nam;
	if(defaultNam)
		nam=new QNetworkAccessManager(0);
	
	// check file size with header:
	int fileSize = getContentLength( url, nam);
	// make sure that not more bytes are requested than are actually there ;-)
	if( fileSize < end)
		end = fileSize - 1;

	
	QByteArray content;
	Downloader *d=new Downloader(content);
	QNetworkRequest req(url);
	if((url.scheme() == "http" || url.scheme() == "https") && (!url.userName().isEmpty() || !url.password().isEmpty()))
		req.setRawHeader("Authorization", "Basic " + QString(url.userName() + ":" + url.password()).toAscii().toBase64());
	if((url.scheme() == "http" || url.scheme() == "https") && (start != 0 || end != -1))
		req.setRawHeader("Range", QString("bytes=" + QString::number(start) + "-" + (end>0 ? QString::number(end) : QString::null)).toAscii());
	
	d->_reply=nam->get(req);
	connect(d->_reply, SIGNAL(finished()), d, SLOT(downloadFinished()));
	if(progressObject && progressSlot)
		ASSERT(connect(d->_reply, SIGNAL(downloadProgress(qint64,qint64)), progressObject, progressSlot));
	d->exec();
	
	truncate(content, d->_reply, start, end);
	
	delete d->_reply;
	d->_reply=0;
	if(defaultNam)
		delete nam;
	
	return content;
}

QByteArray Downloader::get(QUrl const &url, DownloaderObserver *observer, QNetworkAccessManager *nam, int start, int end) {
	bool defaultNam=!nam;
	if(defaultNam)
		nam=new QNetworkAccessManager(0);
	
	QByteArray content;
	Downloader *d=new Downloader(content);
	
	// check file size with header:
	int fileSize = getContentLength( url, nam);
	if( observer != NULL )
	{
		observer->setFileSize( fileSize );
	}
	// make sure that not more bytes are requested than are actually there ;-)
	if( fileSize < end)
		end = fileSize - 1;
	
	QNetworkRequest req(url);
	if((url.scheme() == "http" || url.scheme() == "https") && (!url.userName().isEmpty() || !url.password().isEmpty()))
		req.setRawHeader("Authorization", "Basic " + QString(url.userName() + ":" + url.password()).toAscii().toBase64());
	if((url.scheme() == "http" || url.scheme() == "https") && (start != 0 || end != -1))
		req.setRawHeader("Range", QString("bytes=" + QString::number(start) + "-" + (end>0 ? QString::number(end) : QString::null)).toAscii());
	
	d->_reply=nam->get(req);
	connect(d->_reply, SIGNAL(finished()), d, SLOT(downloadFinished()));
	
	if(observer != NULL)
	{
		d->addObserver( observer );
		ASSERT(connect(d->_reply, SIGNAL(downloadProgress(qint64,qint64)), d, SLOT(updateDownloadProgress( qint64, qint64))));
	}
	d->exec();
	truncate(content, d->_reply, start, end);
	
	delete d->_reply; d->_reply=0;
	if(defaultNam)
		delete nam;
	
	return content;
}



int Downloader::getContentLength(QUrl const &url, QNetworkAccessManager *nam)
{
	bool defaultNam=!nam;
	if(defaultNam)
		nam=new QNetworkAccessManager(0);

	QNetworkRequest req(url);
	if((url.scheme() == "http" || url.scheme() == "https") && (!url.userName().isEmpty() || !url.password().isEmpty()))
		req.setRawHeader("Authorization", "Basic " + QString(url.userName() + ":" + url.password()).toAscii().toBase64());
	QEventLoop l;
	QNetworkReply* reply=nam->head(req);
	ASSERT( connect(reply, SIGNAL(finished()), (QObject*)&l, SLOT(quit())) );
	l.exec();	
	
	int fS = reply->header(QNetworkRequest::ContentLengthHeader).toInt();	
	if(defaultNam)
		delete nam;
	
	return fS;
}


bool Downloader::download(QUrl const &url, QString const &target, QNetworkAccessManager *nam, QObject *progressObject, char const * const progressSlot, int start, int end) {
	QByteArray content=get(url, nam, progressObject, progressSlot, start, end);
	if(content.isEmpty())
		return false;
	QFile f(target);
	if(!f.open(QFile::WriteOnly|(start ? QFile::Append : QFile::Truncate)))
		return false;
	f.write(content);
	return true;
}


bool Downloader::download(QUrl const &url, QString const &target, DownloaderObserver *observer, QNetworkAccessManager *nam, int start, int end) {
	QByteArray content=get(url, observer, nam, start, end);
	if(content.isEmpty())
		return false;
	QFile f(target);
	if(!f.open(QFile::WriteOnly|(start ? QFile::Append : QFile::Truncate)))
		return false;
	f.write(content);
	return true;
}


bool Downloader::download(QUrl const &url, QString const &target, QHash<QByteArray, QByteArray> const &postParameters, DownloaderObserver *observer, QNetworkAccessManager *nam, int packageSize, int start, int end) 
{	
	QFile f(target);
	if(!f.open(QFile::WriteOnly|(start ? QFile::Append : QFile::Truncate)))
		return false;
	
	if( observer == NULL )
	{
		observer = new DownloaderObserver();
	}
	
	qDebug() <<"Packaged-Download with Post from "<<start << " to " <<end;
	
	
	observer->_status = DownloaderObserver::STARTED;
	observer->_downloadInProgress = true;
	
	int packageStart = start;
	int packageEnd = start + packageSize;
	int received = 0;
	do
	{
		qDebug() <<"-- Package Post from "<<packageStart << " -> " <<packageEnd;
		QByteArray content=getWithPost(url, postParameters, observer, nam, packageStart, packageEnd);
		if( end == -1 )
		{
			end = observer->getFileSize();
		}
		received += content.length();
		qDebug() <<"-- Package bytes received: "<<received;
		if(!content.isEmpty())
		{
			f.write(content);
		}
		else
		{
			return false;
		}
		
		// check if the download was paused/cancelled
		if( ( observer->downloadStatus() == DownloaderObserver::PAUSED ) || ( observer->downloadStatus() == DownloaderObserver::CANCELED ) )
		{
			//qDebug() << "Download paused or canceled, so packaged downloading stopped";
			break;
		}
		
		packageStart = packageEnd + 1;
		packageEnd = packageStart + packageSize;
	}
	while(  (start + received) < end );	
	f.close();
	return true;
}



bool Downloader::download(QUrl const &url, QString const &target, DownloaderObserver *observer, QNetworkAccessManager *nam, int packageSize, int start, int end) 
{	
	QFile f(target);
	if(!f.open(QFile::WriteOnly|(start ? QFile::Append : QFile::Truncate)))
		return false;
	
	if( observer == NULL )
	{
		observer = new DownloaderObserver();
	}
	
	qDebug() <<"Packaged-Download from "<<start << " to " <<end;

	
	observer->_status = DownloaderObserver::STARTED;
	observer->_downloadInProgress = true;

	int packageStart = start;
	int packageEnd = start + packageSize;
	int received = 0;
	do
	{
		qDebug() <<"-- Package from "<<packageStart << " -> " <<packageEnd;
		QByteArray content=get(url, observer, nam, packageStart, packageEnd);	
		if( end == -1 )
		{
			end = observer->getFileSize();
		}
		received += content.length();
		qDebug() <<"-- Package bytes received: "<<received;
		if(!content.isEmpty())
		{
			f.write(content);
		}
		else
		{
			qDebug() << "Stop download, something went wrong...";
			return false;
		}
				
		// check if the download was paused/cancelled
		if( ( observer->downloadStatus() == DownloaderObserver::PAUSED ) || ( observer->downloadStatus() == DownloaderObserver::CANCELED ) )
		{
			break;
		}
		
		packageStart = packageEnd + 1;
		packageEnd = packageStart + packageSize;
	}
	while(  (start + received) < end );	
	f.close();
	return true;
}

QByteArray Downloader::getWithPost(QUrl const &url, QHash<QByteArray,QByteArray> const &postParameters, QNetworkAccessManager *nam, QObject *progressObject, char const * const progressSlot, int start, int end) {
	bool defaultNam=!nam;
	if(defaultNam)
		nam=new QNetworkAccessManager(0);
	
	QByteArray content;
	Downloader *d=new Downloader(content);
		
	// check file size with header:
	int fileSize = getContentLength( url, nam);
	// make sure that not more bytes are requested than are actually there ;-)
	if( fileSize < end)
		end = fileSize - 1;
	
	QNetworkRequest req(url);
	if((url.scheme() == "http" || url.scheme() == "https") && (!url.userName().isEmpty() || !url.password().isEmpty()))
		req.setRawHeader("Authorization", "Basic " + QString(url.userName() + ":" + url.password()).toAscii().toBase64());
	if((url.scheme() == "http" || url.scheme() == "https") && (start != 0 || end != -1))
		req.setRawHeader("Range", QString("bytes=" + QString::number(start) + "-" + (end>0 ? QString::number(end) : QString::null)).toAscii());
	QByteArray post;
	for(QHash<QByteArray,QByteArray>::ConstIterator it=postParameters.constBegin(); it!=postParameters.constEnd(); it++) {
		if(!post.isEmpty())
			post.append('&');
		post.append(it.key());
		post.append('=');
		post.append(it.value());
	}
	d->_reply=nam->post(req, post);
	connect(d->_reply, SIGNAL(finished()), d, SLOT(downloadFinished()));	
	if(progressObject && progressSlot)
		ASSERT(connect(d->_reply, SIGNAL(downloadProgress(qint64,qint64)), progressObject, progressSlot));
	d->exec();
	
	truncate(content, d->_reply, start, end);
	
	delete d->_reply; d->_reply=0;
	if(defaultNam)
		delete nam;
	
	return content;
}


QByteArray Downloader::getWithPost(QUrl const &url, QHash<QByteArray,QByteArray> const &postParameters, DownloaderObserver *observer, QNetworkAccessManager *nam, int start, int end) {
	bool defaultNam=!nam;
	if(defaultNam)
		nam=new QNetworkAccessManager(0);
	
	QByteArray content;
	Downloader *d=new Downloader(content);
	
	// check file size with header:
	int fileSize = getContentLength( url, nam);
	if( observer != NULL )
	{
		observer->setFileSize( fileSize );
	}

	// make sure that not more bytes are requested than are actually there ;-)
	if( fileSize < end)
	{
		qDebug() << "Adjust download end point from "<<end<<" to filesize " <<observer->getFileSize();
		end = fileSize - 1;
	}
	
	QNetworkRequest req(url);
	if((url.scheme() == "http" || url.scheme() == "https") && (!url.userName().isEmpty() || !url.password().isEmpty()))
		req.setRawHeader("Authorization", "Basic " + QString(url.userName() + ":" + url.password()).toAscii().toBase64());
	if((url.scheme() == "http" || url.scheme() == "https") && (start != 0 || end != -1))
		req.setRawHeader("Range", QString("bytes=" + QString::number(start) + "-" + (end>0 ? QString::number(end) : QString::null)).toAscii());
	QByteArray post;
	for(QHash<QByteArray,QByteArray>::ConstIterator it=postParameters.constBegin(); it!=postParameters.constEnd(); it++) {
		if(!post.isEmpty())
			post.append('&');
		post.append(it.key());
		post.append('=');
		post.append(it.value());
	}
	d->_reply=nam->post(req, post);
	connect(d->_reply, SIGNAL(finished()), d, SLOT(downloadFinished()));	
	
	if(observer) {
		d->addObserver( observer );
		ASSERT(connect(d->_reply, SIGNAL(downloadProgress(qint64,qint64)), d, SLOT(updateDownloadProgress( qint64, qint64))));
	}
	
	d->exec();
	
	truncate(content, d->_reply, start, end);

	delete d->_reply; d->_reply=0;
	if(defaultNam)
		delete nam;
	
	return content;
}



void Downloader::updateDownloadProgress( qint64 bytesReceived, qint64 totalBytes)
{	
	foreach( DownloaderObserver* o, _observers) 
	{
		if(!o->updateDownloadProgress( bytesReceived, totalBytes )) {
			// read the content before aborting the request
			_target=_reply->readAll();
			_reply->abort();
			downloadFinished();
		}
		
	}
}


void Downloader::addObserver(DownloaderObserver *observer)
{
	if( observer )
		_observers.append(observer);
}
void Downloader::removeObserver(DownloaderObserver *observer)
{
	_observers.removeAll(observer);
}

