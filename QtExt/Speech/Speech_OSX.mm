#include "Speech"

#import <NSSpeechSynthesizer.h>
#import <NSString.h>

using namespace std;

static NSSpeechSynthesizer *instance = 0;

void Speech::say(QString text)
{
	if(!instance) {
		instance = [[NSSpeechSynthesizer alloc] init];
	}

	[instance startSpeakingString:[NSString stringWithUTF8String:text.toUtf8().data()]];
}

Speech::Speech():QThread(0)
{
}

void Speech::stopCurrent()
{
	[instance stopSpeaking];
}

void Speech::stop()
{
	[instance stopSpeaking];
}

void Speech::run()
{
}
