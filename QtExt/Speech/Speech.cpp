#include "Speech"

#include <QWaitCondition>
#include <QMutex>
#include <QIOStream>

#include <festival.h>

extern "C" {
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
}

using namespace std;

static Speech *instance = 0;
static QVector<EST_Wave> waves;
static bool stereo_only = false;
static QMutex waveLock;
static QWaitCondition speechQueue;
static bool stopFlag = false;

static int sb_set_sample_rate(int dev, int rate)
{
	int fmt, sfmts, stereo=0, sstereo, channels=1;
	ioctl(dev, SNDCTL_DSP_RESET, 0);
	ioctl(dev, SNDCTL_DSP_SPEED, &rate);
	sstereo = stereo;
	ioctl(dev, SNDCTL_DSP_STEREO, &sstereo);
	/* Some devices don't do mono even when asked to */
	if(sstereo != stereo)
		stereo_only = true;
	ioctl(dev, SNDCTL_DSP_CHANNELS, &channels);
	ioctl(dev, SNDCTL_DSP_GETFMTS, &sfmts);
	if(sfmts == AFMT_U8)
		fmt = AFMT_U8;	// 8-bit only device
	else if(EST_LITTLE_ENDIAN)
		fmt = AFMT_S16_LE;
	else
		fmt = AFMT_S16_BE;
	ioctl(dev, SNDCTL_DSP_SETFMT, &fmt);
	return fmt;
}

#define AUDIOBUFFSIZE 256

void Speech::say(QString text)
{
	if(!instance)
		instance=new Speech;

	waveLock.lock();
	EST_Wave wave;
	festival_text_to_wave(text.toLatin1().data(), wave);
	waves.append(wave);
	waveLock.unlock();
	if(!instance->isRunning()) {
		instance->start();
		while(!instance->isRunning())
			usleep(100000);
	}
	speechQueue.wakeAll();
}

Speech::Speech():QThread(0)
{
	festival_initialize(1 /* load init files */, 1024000 /* heap size */);
}

void Speech::stopCurrent()
{
	stopFlag=true;
}

void Speech::stop()
{
	waveLock.lock();
	waves.clear();
	stopFlag=true;
	waveLock.unlock();
	speechQueue.wakeAll();
}

void Speech::run()
{
	while(true) {
		waveLock.lock();
		if(waves.isEmpty()) {
			speechQueue.wait(&waveLock);
		}
		stopFlag=false;
		if(waves.isEmpty()) {
			waveLock.unlock();
			continue;
		}
		int audio = open("/dev/dsp", O_WRONLY);
		if(audio<0) {
			perror("/dev/dsp");
			waveLock.unlock();
			continue;
		}
		EST_Wave &current=waves.first();
		waveLock.unlock();
		short *waveform = current.values().memory();
		short *waveform2 = 0;
		int num_samples = current.num_samples();
		int sample_rate = current.sample_rate();
		int actual_fmt = sb_set_sample_rate(audio, sample_rate);
		
		if(stereo_only) {
			waveform2 = walloc(short, num_samples*2);
			for(int i=0; i<num_samples; ++i)
				waveform2[i*2] = waveform2[(i*2)+1] = current.a(i);
			waveform=waveform2;
			num_samples *= 2;
		}

		if(actual_fmt == AFMT_U8) {
			// It's actually 8 bit unsigned - we have to convert
			// the buffer
			unsigned char *uchars = walloc(unsigned char, num_samples);
			for(int i=0; i<num_samples; ++i)
				uchars[i] = waveform[i]/256+128;

			int r=0, n;
			for(int i=0; i<num_samples; i += r) {
				if(num_samples > i+AUDIOBUFFSIZE)
					n = AUDIOBUFFSIZE;
				else
					n = num_samples-1;
				r = write(audio, &uchars[i], n);
				if (r == 0)
					break;
				if(stopFlag) {
					stopFlag=false;
					break;
				}
			}
			wfree(uchars);
		} else if(actual_fmt == AFMT_S16_LE || actual_fmt == AFMT_S16_BE) {
			int nbuf, c;
			short *buf;
			ioctl(audio, SNDCTL_DSP_GETBLKSIZE, &nbuf);
			buf = new short[nbuf];
			if(nbuf > 1024) // Keep the buffer size low so we can interupt in a timely manner
				nbuf = 1024;
			int r=0, n;
			for(int i=0; i<num_samples; i += r/2) {
				if(num_samples > i+nbuf)
					n = nbuf;
				else
					n = num_samples-i;
				int c;
				for(c=0; c<n; c++)
					buf[c]=waveform[c+i];
				for(;c<nbuf;c++)
					buf[c]=waveform[n-1];
				r=write(audio, buf, nbuf*2);
				if(r<=0) {
					perror("Write to dsp");
					break;
				}
				if(stopFlag) {
					stopFlag=false;
					break;
				}
			}
			delete[] buf;
		} else
			cerr << "Unsupported sample rate " << sample_rate << endl;
		if(waveform2)
			wfree(waveform2);
		waveLock.lock();
		waves.pop_front();
		waveLock.unlock();
		::close(audio);
	}
}
