#ifndef UPLOADER_H
#define UPLOADER_H

#include <QHash>
#include <QNetworkAccessManager>
#include "CompilerTools"

class UploaderObserver;

/**
 * @class Uploader
 * @brief A simple HTTP form uploader
 * @author Víctor Fernández Martínez <victor.fernandez@blankpage.ch>
 * @date 2011
 *
 * This class provides a simple HTTP forms uploader using
 * QNetworkManager as its backend.
 */

typedef QHash<QString, QVariant> StringVariantHash;
typedef QHash<QString, QString> StringStringHash;
typedef QHash<QString, QPair<QString, QIODevice*> > StringPairHash;

class UploaderPrivate;
class QTEXT_EXPORT Uploader
{
public:
	Uploader(QUrl const& url = QUrl(),
			 StringVariantHash const& fields = StringVariantHash(),
			 StringStringHash const& files = StringStringHash(),
			 StringPairHash const& devices = StringPairHash());
	QByteArray blockingPost();
	void insertField(QString const& fieldName, QVariant const& value);
	void insertFile(QString const& fieldName, QString const& filePath);
	void insertIODevice(QString const& fieldName, QString const& fileName, QIODevice *device);
	void setCookie(QHash<QByteArray, QByteArray> const& cookie);
	void setNetworkAccessManager(QNetworkAccessManager *manager);
	void setRawHeader(QByteArray const& headerName, QByteArray const& headerValue);
	void setUrl(QUrl const& url);
	QNetworkReply* post();

	/**
	 * Add an observer to get updates about download progress
	 * @param observer to add 
	 */
	void addObserver(UploaderObserver *observer);

	/**
	 * Remove an observer
	 * @param observer to remove 
	 */
	void removeObserver(UploaderObserver *observer);

	static QByteArray post(QUrl const& url,
						   StringVariantHash const& fields,
						   StringStringHash const& files = StringStringHash(),
						   StringPairHash const& devices = StringPairHash());

private:
	UploaderPrivate * const d;
};

#endif
