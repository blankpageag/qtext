#include <QFile>
#include <QStringList>
#include <cstdio>
#include "Accelerometer"

Accelerometer *Accelerometer::_instance = 0;

Accelerometer *Accelerometer::instance() {
	if(!_instance) {
		if(Lis3lv02dAccelerometer::exists())
			_instance=new Lis3lv02dAccelerometer;
	}
	return _instance;
}

int Accelerometer::x(bool update) {
	if(update)
		updatePosition();
	return _x;
}

int Accelerometer::y(bool update) {
	if(update)
		updatePosition();
	return _y;
}

int Accelerometer::z(bool update) {
	if(update)
		updatePosition();
	return _z;
}

Accelerometer::Orientation Accelerometer::orientation(bool update) {
	Orientation result = 0;
	if(update)
		updatePosition();
	if(_x > -500 && _x < 500)
		result = Landscape;
	else
		result = Portrait;
	if(_z < -50 || _x > 500)
		result |= UpsideDown;
	return result;
}

Lis3lv02dAccelerometer::Lis3lv02dAccelerometer():Accelerometer(),_updateTimer(0),_trackers(0) {
}

bool Lis3lv02dAccelerometer::exists() {
	return QFile::exists("/sys/devices/platform/lis3lv02d/position");
}

void Lis3lv02dAccelerometer::startTracking(int pollInterval) {
	if(++_trackers == 1) {
		_updateTimer=new QTimer(this);
		_updateTimer->setInterval(pollInterval);
		connect(_updateTimer, SIGNAL(timeout()), this, SLOT(updatePosition()));
		_updateTimer->start();
	} else if(pollInterval < _updateTimer->interval()) {
		// We need more precision...
		_updateTimer->setInterval(pollInterval);
	}
}

void Lis3lv02dAccelerometer::stopTracking() {
	if(--_trackers == 0) {
		delete _updateTimer;
		_updateTimer->stop();
	}
}


void Lis3lv02dAccelerometer::updatePosition() {
	FILE *f=fopen("/sys/devices/platform/lis3lv02d/position", "r");
	if(!f)
		return;
	char buf[128];
	fgets(buf, 128, f);
	QString l=QString(buf).trimmed();
	if(!l.startsWith('(') || !l.endsWith(')')) {
		fclose(f);
		return; // maybe we should try again instead?
	}
	QStringList coordinates=l.mid(1, l.length()-2).split(',');
	int x=coordinates[0].toInt();
	int y=coordinates[1].toInt();
	int z=coordinates[2].toInt();
	if(x != _x || y != _y || z != _z) {
		Orientation o=orientation(false);
		_x=x; _y=y; _z=z;
		emit positionChanged(x, y, z);
		Orientation no=orientation(false);
		if(o != no)
			emit orientationChanged(no);
	}
	fclose(f);
}
