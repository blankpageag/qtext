#include <DownloaderObserver>

#include <QDebug>

DownloaderObserver::DownloaderObserver():downloadProgressCallBackPtr(NULL),_downloadedBytes(0),_downloadedTmpBytes(0), _totalNumberOfBytes(0), _fileSize(0), _downloadInProgress(true), _status( DownloaderObserver::NOTSTARTED)
{}

bool DownloaderObserver::updateDownloadProgress(qint64 bytesReceived, qint64 totalBytes)
{	_totalNumberOfBytes = totalBytes;
	_downloadedTmpBytes = bytesReceived;
	
	if( downloadProgressCallBackPtr != NULL )
	{
		downloadProgressCallBackPtr( downloadedBytes() + _downloadedTmpBytes, getFileSize());
	}
	
	emit downloadProgressed( downloadedBytes() + _downloadedTmpBytes, getFileSize() );
	return _downloadInProgress;
}

void DownloaderObserver::storeDownload() 
{
	_downloadedBytes += _downloadedTmpBytes;
}


void DownloaderObserver::stopDownload() 
{
	_status = DownloaderObserver::PAUSED;
	_downloadedBytes += _downloadedTmpBytes;
	_downloadInProgress = false;
}

void DownloaderObserver::setDownloadProgressCallBack(void (*callback)(int, int))
{
	downloadProgressCallBackPtr=callback;
}

unsigned int DownloaderObserver::downloadedBytes()
{
	return _downloadedBytes;
}

unsigned int DownloaderObserver::totalNumberOfBytes()
{
	return _totalNumberOfBytes;
}

void DownloaderObserver::setFileSize(int iSize)
{
	_fileSize = iSize;
}

int DownloaderObserver::getFileSize()
{
	return _fileSize;
}

void DownloaderObserver::resetDownload()
{
	_status = DownloaderObserver::NOTSTARTED;
	_totalNumberOfBytes = 0;
	_downloadedBytes = 0;
	_downloadedTmpBytes = 0;
	_fileSize = 0;
	_downloadInProgress = true;
}
