#include "DirTools"
#include <QFile>
#include <QFileInfo>
#include <QIOStream>

extern "C" {
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
}

bool DirTools::mkdir(QString const &name, mode_t mode, bool withParents)
{
	if(!withParents)
#ifdef Q_OS_WIN
		return QDir::root().mkdir(name);
#else
		return !::mkdir(QFile::encodeName(name).data(), mode);
#endif

	QString cur;
	QStringList bits=name.split('/');
	bool result=false;
	foreach(QString const &bit, bits) {
		cur += bit + "/";
#ifdef Q_OS_WIN
		// Windoze doesn't support permissions, so we can just do this...
		result=QDir::root().mkdir(cur);
#else
		result=!::mkdir(QFile::encodeName(cur).data(), mode);
#endif
	}
	return result;
}

QStringList DirTools::find(QDir const &dir, QString const &filename)
{
	QStringList results;
	QStringList e=dir.entryList(QStringList() << filename, QDir::Files);
	foreach(QString const &entry, e) {
		results.append(dir.absolutePath() + "/" + entry);
	}
	QStringList dirs=dir.entryList(QStringList(), QDir::Dirs|QDir::NoDotAndDotDot);
	foreach(QString const &subdir, dirs) {
		results += find(QString(dir.absolutePath() + "/" + subdir), filename);
	}
	return results;
}

QString DirTools::findFirst(QDir const &dir, QString const &filename)
{
	QString result;
	QStringList e=dir.entryList(QStringList() << filename, QDir::Files);
	if(!e.isEmpty())
		return dir.absolutePath() + "/" + e.first();
	QStringList dirs=dir.entryList(QStringList(), QDir::Dirs|QDir::NoDotAndDotDot);
	foreach(QString const &subdir, dirs) {
		result=findFirst(QString(dir.absolutePath() + "/" + subdir), filename);
		if(!result.isEmpty())
			return result;
	}
	return QString::null;
}

bool DirTools::writable(QString const &name)
{
#ifdef Q_OS_WIN
	return QFile::permissions(name) & QFile::WriteUser;
#else
	// On a real OS, we check permissions with access() because
	// we may have write access through something other than
	// our user ID -- e.g. the directory might be writable by
	// our group
	return !access(QFile::encodeName(name).data(), W_OK);
#endif
}

bool DirTools::remove(QString const &name)
{
	if(!QFileInfo(name).isDir())
		return QFile::remove(name);

	QDir d(name);
	if(!d.exists())
		return false;

	bool success=true;
	QFileInfoList entries = d.entryInfoList(QDir::NoDotAndDotDot|QDir::Dirs|QDir::Files|QDir::Hidden|QDir::System);
	foreach(QFileInfo const &entryInfo, entries) {
		QString const path = entryInfo.absoluteFilePath();
		if(entryInfo.isDir())
			success = success && remove(path);
		else
			success = success && QFile::remove(path);
	}
	QString const p=d.absolutePath();
	d.cdUp();
	success = success && d.rmdir(p);
	return success;
}

bool DirTools::copy(QString const &src, QString const &dest)
{
	if(!QFileInfo(src).isDir())
		return QFile::copy(src, dest);

	QDir s(src);
	if(!s.exists())
		return false;

	if(!QFile::exists(dest))
		mkdir(dest);

	bool success = true;
	QFileInfoList entries = s.entryInfoList(QDir::NoDotAndDotDot|QDir::Dirs|QDir::Files|QDir::Hidden|QDir::System);
	foreach(QFileInfo const &entryInfo, entries) {
		QString const path = entryInfo.absoluteFilePath();
		if(entryInfo.isDir())
			success = success && copy(path, dest + QDir::separator() + entryInfo.fileName());
		else
			success = success && QFile::copy(path, dest + QDir::separator() + entryInfo.fileName());
	}
	return success;
}

namespace {
static QStringList files(QDir const &dir, QString const &prefix=QString::null) {
	QStringList result;
#ifdef QT_DIR_EXCLUSION_IS_FIXED
	QStringList const files = dir.entryList(QDir::Files|QDir::Hidden|QDir::System);
	foreach(QString const &f, files)
		result << (prefix + f);
#else
	// Workaround for a Qt bug: The code fragment above includes directories
	// even though QDir::Dirs isn't given.
	// Bug last verified to be present in Qt 4.7.1-20101005, QTBUG-14257
	QFileInfoList const currentFiles = dir.entryInfoList(QDir::Files|QDir::Hidden|QDir::System);
	foreach(QFileInfo const &f, currentFiles) {
		if(!f.isDir()) {
			if(f.isRelative())
				result << (prefix + f.filePath());
			else
				result << (prefix + f.filePath().mid(dir.absolutePath().length()+1));
		}
	}
#endif
	QStringList const subdirs = dir.entryList(QDir::Dirs|QDir::Hidden|QDir::System|QDir::NoDotAndDotDot);
	foreach(QString const &sd, subdirs) {
		result << files(dir.absoluteFilePath(sd), prefix + sd + "/");
	}
	return result;
}
}

QStringList DirTools::files(QDir const &d) {
	return ::files(d, QString::null);
}

QFileInfoList DirTools::fileInfos(QDir const &dir) {
	QFileInfoList result;
	// Workaround for a Qt bug: The code fragment above includes directories
	// even though QDir::Dirs isn't given.
	// Bug last verified to be present in Qt 4.7.1-20101005, QTBUG-14257
	QFileInfoList const files = dir.entryInfoList(QDir::Files|QDir::Hidden|QDir::System);
	foreach(QFileInfo const &f, files) {
		if(!f.isDir())
			result << f;
	}
	QStringList const subdirs = dir.entryList(QDir::Dirs|QDir::Hidden|QDir::System|QDir::NoDotAndDotDot);
	foreach(QString const &sd, subdirs) {
		result << fileInfos(dir.absoluteFilePath(sd));
	}
	return result;
}

