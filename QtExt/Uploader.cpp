#include <QtNetwork>
#include "Uploader_p.h"
#include "Uploader"

Uploader::Uploader(QUrl const& url, QHash<QString, QVariant> const& fields, QHash<QString, QString> const& files, QHash<QString, QPair<QString, QIODevice*> > const& devices) :
	d(new UploaderPrivate(url, fields, devices))
{
	QHashIterator<QString, QString> it(files);
	while(it.hasNext()) {
		it.next();
		insertFile(it.key(), it.value());
	}
}


QByteArray Uploader::blockingPost()
{
	QEventLoop loop;
	QNetworkReply *reply = post();
	QObject::connect(reply, SIGNAL(finished()),
					 &loop, SLOT(quit()));
	loop.exec();

	if(reply->error() == QNetworkReply::NoError) {
		return reply->readAll();
	} else {
		qDebug() << "Upload failed. Error: " << reply->error();
		return QByteArray();
	}

	reply->deleteLater();
}


void Uploader::insertField(QString const& fieldName, QVariant const& value)
{
	d->_fields.insert(fieldName, value);
}


void Uploader::insertFile(QString const& fieldName, QString const& filePath)
{
	QString fileName = QFileInfo(filePath).fileName();
	QIODevice *device = new QFile(filePath, d);
	QPair<QString, QIODevice*> pair = qMakePair(fileName, device);
	d->_devices.insert(fieldName, pair);
}


void Uploader::insertIODevice(QString const& fieldName, QString const& fileName, QIODevice *device)
{
	d->_devices.insert(fieldName, qMakePair(fileName, device));
}


void Uploader::setNetworkAccessManager(QNetworkAccessManager *manager)
{
	d->_accessManager = manager;
}


void Uploader::setCookie(QHash<QByteArray, QByteArray> const& cookie)
{
	QByteArray contents;
	QHashIterator<QByteArray, QByteArray> it(cookie);
	while(it.hasNext()) {
		it.next();
		if(!contents.isEmpty()) {
			contents += "; ";
		}
		contents += it.key() + "=" + it.value();
	}
	setRawHeader("Cookie", contents);
}


void Uploader::setRawHeader(QByteArray const& headerName, QByteArray const& headerValue)
{
	d->_request.setRawHeader(headerName, headerValue);
}


void Uploader::setUrl(QUrl const& url)
{
	d->_request.setUrl(url);
}


QNetworkReply* Uploader::post()
{
	d->_request.setRawHeader("Content-Type", QByteArray("multipart/form-data; boundary=").append(d->_boundary));

	QNetworkAccessManager *accessManager = d->_accessManager;
	if(!accessManager) {
		if(!d->_privateAccessManager) {
			d->_privateAccessManager = new QNetworkAccessManager(d);
		}
		accessManager = d->_privateAccessManager;
	}

	if(d->open(QIODevice::ReadOnly)) {
		QNetworkReply *reply = accessManager->post(d->_request, d);
		QObject::connect(reply, SIGNAL(uploadProgress(qint64, qint64)),
						 d, SLOT(reportProgress(qint64, qint64)));
		return reply;
	} else {
		return 0;
	}
}


void Uploader::addObserver(UploaderObserver *observer)
{
	d->_observers.insert(observer);
}


void Uploader::removeObserver(UploaderObserver *observer)
{
	d->_observers.remove(observer);
}


QByteArray Uploader::post(QUrl const& url, QHash<QString, QVariant> const& fields, QHash<QString, QString> const& files, QHash<QString, QPair<QString, QIODevice*> > const& devices)
{
	return Uploader(url, fields, files, devices).blockingPost();
}
