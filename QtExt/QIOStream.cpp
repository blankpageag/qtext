#include "QIOStream"
#include <QStringList>
#include <QDateTime>
#include <QUrl>

#ifndef _MSC_VER
#include <stdint.h>
#else
#pragma message("Your compiler is a dreadful, atrocious abomination that isn't ISO compliant")
typedef quint8 uint8_t;
typedef quint64 uint64_t;
#endif

#ifndef WITHOUT_STL
using namespace std;

ostream &operator <<(ostream &os, QString const &s) {
	return os << s.toLocal8Bit().data();
}

ostream &operator <<(ostream &os, QUrl const &u) {
	return os << u.toString();
}

ostream &operator <<(ostream &os, QFileInfo const &s) {
	return os << s.absoluteFilePath();
}

ostream &operator <<(ostream &os, QDate const &d) {
	return os << d.toString();
}

ostream &operator <<(ostream &os, QTime const &t) {
	return os << t.toString();
}

ostream &operator <<(ostream &os, QDateTime const &dt) {
	return os << dt.toString();
}

ostream &operator <<(ostream &os, QRegExp const &rx) {
	return os << rx.pattern();
}

ostream &operator <<(ostream &os, QPoint const &p) {
	return os << "(" << p.x() << "," << p.y() << ")";
}

ostream &operator <<(ostream &os, QPointF const &p) {
	return os << "(" << p.x() << "," << p.y() << ")";
}

ostream &operator <<(ostream &os, QSize const &s) {
	return os << s.width() << "x" << s.height();
}

ostream &operator <<(ostream &os, QRect const &r) {
	return os << "(" << r.left() << "," << r.top() << ")-(" << r.right() << "," << r.bottom() << ")";
}

ostream &operator <<(ostream &os, QRectF const &r) {
	return os << "(" << r.left() << "," << r.top() << ")-(" << r.right() << "," << r.bottom() << ")";
}

ostream &operator <<(ostream &os, QVariant const &v) {
	if(v.type() == QVariant::Bool)
		os << (v.toBool() ? "true" : "false");
	else if(v.type() == QVariant::Char)
		os << v.toChar().toLatin1();
	else if(v.type() == QVariant::Date)
		os << v.toDate();
	else if(v.type() == QVariant::DateTime)
		os << v.toDateTime();
	else if(v.type() == QVariant::Double)
		os << v.toDouble();
	else if(v.type() == QVariant::Int)
		os << v.toInt();
	else if(v.type() == QVariant::String)
		os << v.toString();
	else if(v.type() == QVariant::StringList)
		os << v.toStringList();
	else if(v.type() == QVariant::Time)
		os << v.toTime();
	else if(v.type() == QVariant::UInt)
		os << v.toUInt();
	else if(v.type() == QVariant::ULongLong)
		os << v.toULongLong();
	else if(v.type() == QVariant::Url)
		os << v.toUrl().toString();
	else if(v.type() == QVariant::RegExp)
		os << v.toRegExp();
	else if(v.canConvert<QVariantList>()) {
		os << v.toList();
	}
	else if(v.canConvert<QString>())
		os << v.toString();
	else if(v.canConvert<uint64_t>())
		os << v.toULongLong();
	return os;
}

ostream &operator <<(ostream &os, QByteArray const &a) {
	return os << hexdump(a);
}
#endif

QString hexdump(QByteArray const &data, int size) {
	if(size<0)
		size=data.count();
	return hexdump(data.data(), static_cast<unsigned int>(size));
}

QString hexdump(void const * const data, unsigned int size) {
	uint8_t const * const d=static_cast<uint8_t const * const>(data);
	QString s, bin;
	for(unsigned int i=0; i<size; ++i) {
		if(i && (i%16 == 0)) {
			s += "\t" + bin + "\n";
			bin="";
		}
		s += hex(d[i]) + " ";
		if(isprint(d[i]))
			bin += QChar(d[i]);
		else
			bin += ".";
	}
	if(!bin.isEmpty()) {
		for(int i=0; i<47-3*bin.length(); i++)
			s += " ";
		s += " \t" + bin + "\n";
	}
	return s;
}
