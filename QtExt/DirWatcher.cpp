#include "DirWatcher"
#include <QFileInfo>
#include <QHash>

#ifdef HAVE_INOTIFY
extern "C" {
#include <sys/inotify.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
}

DirWatcher::DirWatcher(QObject *parent):QObject(parent),_dir(),_watchPoint(-1)
{
	init();
}

DirWatcher::DirWatcher(QString const &dir, QObject *parent):QObject(parent),_dir(),_watchPoint(-1)
{
	init();
	watch(dir);
}

DirWatcher::~DirWatcher()
{
	if(_inotifyFd>=0)
		::close(_inotifyFd);
}

void DirWatcher::init()
{
	_inotifyFd=inotify_init();
	fcntl(_inotifyFd, F_SETFD, FD_CLOEXEC);
	_inotifyWatcher=new QSocketNotifier(_inotifyFd, QSocketNotifier::Read, this);
	connect(_inotifyWatcher, SIGNAL(activated(int)), this, SLOT(inotifyData()));
	_inotifyWatcher->setEnabled(true);
}

void DirWatcher::watch(QString const &dir)
{
	_dir=dir;
	if(_watchPoint>=0)
		inotify_rm_watch(_inotifyFd, _watchPoint);
	_watchPoint=inotify_add_watch(_inotifyFd, QFile::encodeName(_dir), IN_CREATE|IN_DELETE|IN_DELETE_SELF|IN_MOVE_SELF|IN_MOVED_FROM|IN_MOVED_TO|IN_MODIFY);
}

void DirWatcher::inotifyData()
{
	int pending=-1, pos=0;
	ioctl(_inotifyFd, FIONREAD, &pending);
	char buf[pending];
	read(_inotifyFd, buf, pending);
	bool needRefresh=false;
	QHash<uint32_t,QString> movedFrom;
	QHash<uint32_t,QString> movedTo;
	while(pending>0) {
		inotify_event *e=reinterpret_cast<inotify_event*>(buf+pos);
		pending -= sizeof(inotify_event) + e->len;
		pos += sizeof(inotify_event) + e->len;
		if(!(e->mask & IN_IGNORED) && /* ignore hidden files too */ e->name[0] != '.')
			needRefresh=true;
		// Let's try to be smart about detecting renames etc. so e.g.
		// thumbnail handling can be smarter than deleting the old
		// file name's thumbnail and generating a new one for the new
		// file...
		if(e->mask & IN_DELETE)
			emit fileDeleted(e->name);
		else if(e->mask & IN_MOVED_FROM)
			movedFrom.insert(e->cookie, e->name);
		else if(e->mask & IN_MOVED_TO)
			movedTo.insert(e->cookie, e->name);
		else if(e->mask & IN_CREATE)
			emit fileCreated(e->name);
		else if(e->mask)
			emit fileModified(e->name);
	}
	for(QHash<uint32_t,QString>::ConstIterator mf=movedFrom.constBegin(); mf!=movedFrom.constEnd(); mf++) {
		if(!movedTo.contains(mf.key())) {
			// Present in movedFrom, but not movedTo --> as far as
			// we're concerned here, the file was deleted.
			emit fileDeleted(mf.value());
			continue;
		}
		emit fileRenamed(mf.value(), movedTo[mf.key()]);
	}
	if(needRefresh)
		emit refreshNeeded(_dir);
}
#else
#ifdef _MSC_VER
#pragma message("Your compiler is a piece of crap that doesn't support #warning")
#pragma message("DirWatcher is fully functional only on Linux")
#else
#warning DirWatcher is fully functional only on Linux >= 2.6.18
#endif
DirWatcher::DirWatcher(QObject *parent):QObject(parent),_fsWatcher(this)
{
	init();
}

DirWatcher::DirWatcher(QString const &dir, QObject *parent):QObject(parent),_fsWatcher(this)
{
	init();
	watch(dir);
}

DirWatcher::~DirWatcher()
{
}

void DirWatcher::init()
{
	connect(&_fsWatcher, SIGNAL(fileChanged(QString const &)), this, SIGNAL(fileModified(QString const &)));
	connect(&_fsWatcher, SIGNAL(fileChanged(QString const &)), this, SIGNAL(refreshNeeded(QString const &)));
}

void DirWatcher::watch(QString const &dir)
{
	_fsWatcher.addPath(dir);
}

void DirWatcher::inotifyData()
{
}
#endif
