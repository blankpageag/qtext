export QTDIR="/data/local/qt"
export MAINFOLDER=`pwd`

rm -rf build
mkdir build
cd build

cmake .. $QTARGS -DCMAKE_TOOLCHAIN_FILE="$MAINFOLDER/buildsystems/android.toolchain" -DCMAKE_INSTALL_PREFIX="/data/local/qt" \
-DWITH_SPEECH:BOOL=OFF -DWITH_EXAMPLES:BOOL=OFF -DWITH_NETLINK:BOOL=OFF -DQT_RT_LIBRARY:FILEPATH=-lrt

cd ..

echo "Resulting binaries will go to /data/local/qt so they can be pushed alongside Qt"
